import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { CelularesPage } from '../pages/celulares/celulares';
import { ElectroHogarPage } from '../pages/electro-hogar/electro-hogar';
import { PlpPage } from '../pages/plp/plp';
import { HomePage } from '../pages/home/home';
import { TodosPage } from '../pages/todos/todos';
import { AudioPage } from '../pages/audio/audio';
import { TvvjPage } from '../pages/tvvj/tvvj';
import { MasPage } from '../pages/mas/mas';
import { OffersPage } from '../pages/offers/offers';
import { DownloadPage } from '../pages/download/download';
import { BlockerPage } from '../pages/blocker/blocker';
import { DataTroubleshootingPage } from '../pages/data-troubleshooting/data-troubleshooting';
//import { ModalPage } from '../pages/modal/modal'
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HttpClientModule } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/';

import {  FileTransfer,  FileTransferObject  } from '@ionic-native/file-transfer';  
import {  File  } from '@ionic-native/file';
import { DataProvider } from '../providers/data/data'; 

import { ProgressBarComponent } from '../components/progress-bar/progress-bar';
import { AppVersion } from '@ionic-native/app-version'
import { PhotoViewer } from '@ionic-native/photo-viewer'
import { IonicStorageModule } from '@ionic/storage';


@NgModule({
  declarations: [
    MyApp,
    CelularesPage,
    ElectroHogarPage,
    PlpPage,
    HomePage,
    TodosPage,
    TabsPage,
    AudioPage,
    TvvjPage,
    MasPage,
    OffersPage,
    BlockerPage,
    DownloadPage,
    DataTroubleshootingPage,
    ProgressBarComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    //HTTP,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    CelularesPage,
    ElectroHogarPage,
    PlpPage,
    HomePage,
    TodosPage,
    TabsPage,
    AudioPage,
    TvvjPage,
    MasPage,
    OffersPage,
    DownloadPage,
    BlockerPage,
    DataTroubleshootingPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HTTP,
    FileTransfer,  
    FileTransferObject,  
    File,  
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DataProvider,
    AppVersion,
    PhotoViewer
  ]
})
export class AppModule {}
