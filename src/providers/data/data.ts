import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';

@Injectable()
export class DataProvider {
   productDataStatic = {
      "20198063": {
         "familia": "ELECTRO HOGAR",
         "sku": 20198063,
         "Name": "Licuadora Minipimer Rmin-989W Inox ",
         "Brand": "RECCO",
         "activePrice": "16.990",
         "CMRprice": "14.990",
         "strikeOutPrice": "19.990",
         "tableData": {
            "Descripción": "Licuadora Manual  Inox",
            "Marca": "Recco",
            "Modelo": "Rmin-989w",
            "Tecnología ": "Batidor de Helicoptero",
            "Resolución": "2",
            "Tamaño de Pantalla ": "Acero Inoxidable",
            "Potencia": "400 Watts",
            "Características": "Soporte Para Muro. Vaso:  Capacidad Maxima 700 Ml. Batidora Manual. Vaso Con Tapa Y Cuchillos Desmontables"
         }
      },
      "20219387": {
         "familia": "ELECTRO HOGAR",
         "sku": 20219387,
         "Name": "Cocina 4 Platos ",
         "Brand": "FENSA",
         "activePrice": "119.990",
         "CMRprice": "109.990",
         "strikeOutPrice": "164.990",
         "tableData": {
            "Tipo": "A Gas",
            "Modelo": "F2525t",
            "Origen": "Chile",
            "Color": "Plateado",
            "Número de Quemadores": "4",
            "Capacidad Total ": "66 Lts",
            "Alto": "86,5 Cm",
            "Ancho": "55 Cm",
            "Profundidad": "56 Cm",
            "Peso Neto": "40 KG",
            "Garantía Producto": "1 Año",
            "Material": "Acero Inoxidable",
            "Garantia Del Proveedor": "12 Meses",
            "Horno Autolimpiate": "No",
            "Encendido Electronico": "No",
            "Luz En El Horno": "No"
         }
      },
      "20227267": {
         "familia": "ELECTRO HOGAR",
         "sku": 20227267,
         "Name": "Cocina 4 Platos F2235 ",
         "Brand": "FENSA",
         "activePrice": "129.990",
         "CMRprice": null,
         "strikeOutPrice": "139.990",
         "tableData": {
            "Descripción": "Cocina 4 Quemadores F 2235 Bca Fensa ",
            "Marca": "Fensa",
            "Modelo": "F 2235",
            "Color": "Blanco",
            "Capacidad ": "66 Lts",
            "Potencia": "Categoría Ii2 - 3 (Gl Y/o Gn)",
            "Ancho": "550 MM",
            "Alto": "865 MM",
            "Profundidad": "560 MM",
            "Peso": "33 KG",
            "Sistema": "Sistema de Seguridad Termopar En Horno"
         }
      },
      "20231088": {
         "familia": "ELECTRO HOGAR",
         "sku": 20231088,
         "Name": "Cocina 6 Quemadores Diva 820 Negro  ",
         "Brand": "Mademsa",
         "activePrice": "169.990",
         "CMRprice": null,
         "strikeOutPrice": "219.990",
         "tableData": {

         }
      },
      "20256212": {
         "familia": "ELECTRO HOGAR",
         "sku": 20256212,
         "Name": "Lavadora 9 Kg Wa90H4400Sw/Zs Blanca  ",
         "Brand": "SAMSUNG",
         "activePrice": "189.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Modelo": "Wa90h4400sw/zs",
            "Tipo": "Automatica",
            "Origen": "Tailandia",
            "Certificación": "E-013-03-8660",
            "Alto": "88 Cm",
            "Ancho": "52,5 Cm",
            "Profundidad": "60 Cm",
            "Peso": "31 KG",
            "Material Tambor": "Acero Inoxidable",
            "Capacidad Carga": "9kg",
            "Potencia": "330w",
            "Color": "Blanco"
         }
      },
      "20272439": {
         "familia": "ELECTRO HOGAR",
         "sku": 20272439,
         "Name": "Plancha A Vapor Wiron-1000Cer ",
         "Brand": "WURDEN",
         "activePrice": "8.990",
         "CMRprice": "7.990",
         "strikeOutPrice": "14.990",
         "tableData": {

         }
      },
      "20278337": {
         "familia": "ELECTRO HOGAR",
         "sku": 20278337,
         "Name": "Secadora 7Kg Reverplus 6470 Silver ",
         "Brand": "FENSA",
         "activePrice": "129.990",
         "CMRprice": "119.990",
         "strikeOutPrice": "199.990",
         "tableData": {

         }
      },
      "20297533": {
         "familia": "ELECTRO HOGAR",
         "sku": 20297533,
         "Name": "Freezer Horiz 142Lts Efc14A5Mnw Bco ",
         "Brand": "ELECTROLUX",
         "activePrice": "119.990",
         "CMRprice": null,
         "strikeOutPrice": "139.990",
         "tableData": {
            "Tipo": "Freezers Horizontales",
            "Modelo": "Efc14a5m",
            "Nr Certificado Seguridad": "E-013-03-10339",
            "Nr Certificado Eficiencia": "E-013-01-70293",
            "Eficiencia Y Consumo": "A+",
            "Origen": "China",
            "Color": "-",
            "Capacidad Total ": "142 Lt",
            "Alto": "85 Cm",
            "Ancho": "73 Cm",
            "Profundida": "53,7 Cm",
            "Peso Neto": "-",
            "Garantía Producto": "12 Meses",
            "Material Bandejas": "Metal",
            "Ruedas de Desplazamiento": "Si",
            "Alarma de Puerta": "No",
            "Accesorio": "Incluye Llaves Y Ruedas."
         }
      },
      "20306742": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20306742,
         "Name": "Led 32\" Mglnx3290I ",
         "Brand": "MASTER-G",
         "activePrice": "79.990",
         "CMRprice": null,
         "strikeOutPrice": "159.990",
         "tableData": {
            "Modelo": "Mglnx3290i",
            "Tipo": "Basico",
            "Conexión": "USB",
            "Entradas": "HDMI",
            "Tecnologia": "LED",
            "Resolucion": "HD ",
            "Tamaño de Pantalla": "32\"",
            "Alto": "47 Cm",
            "Ancho": "73.2 Cm",
            "Profundidad": "17 Cm",
            "Peso": "4,7 KG",
            "Color": "Negro"
         }
      },
      "20334601": {
         "familia": "ELECTRO HOGAR",
         "sku": 20334601,
         "Name": "Sandwichera Rsa6150 Color Negro ",
         "Brand": "RECCO",
         "activePrice": "8.990",
         "CMRprice": "7.990",
         "strikeOutPrice": "9.990",
         "tableData": {
            "Marca": "Recco",
            "Modelo": "Rsa-6150",
            "Potencia": "750w",
            "Capacidad": "2 Panes",
            "Luz Indicador Encendido": "Si",
            "Color": "Negro"
         }
      },
      "20340551": {
         "familia": "ELECTRO HOGAR",
         "sku": 20340551,
         "Name": "Hervidor 1.7 Lt Rhe  ",
         "Brand": "RECCO",
         "activePrice": "8.990",
         "CMRprice": "7.990",
         "strikeOutPrice": "9.990",
         "tableData": {
            "Marca": "Recco",
            "Modelo": "Rhe-1.7p",
            "Base Desmontable": "Si",
            "Filtro": "Si",
            "Tapa de Seguridad": "Si",
            "Peso": "0,89 KG",
            "Potencia": "2200w",
            "Capacidad": "1.7 Lt",
            "Material": "Metal",
            "Indicador de Temperatura": "Análogo",
            "Selector de Temperatura": "Si",
            "Panel de Temperatura": "No",
            "Nivel de Agua Visible": "Si",
            "Luz Indicador Encendido": "Si"
         }
      },
      "20340560": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20340560,
         "Name": "Smart TV 32\" RLED-L32D1620SMT/L32D1200SMT ",
         "Brand": "RECCO",
         "activePrice": "99.990",
         "CMRprice": "89.990",
         "strikeOutPrice": "129.990",
         "tableData": {

         }
      },
      "20359286": {
         "familia": "ELECTRO HOGAR",
         "sku": 20359286,
         "Name": "Lavadora Wa13J5730Ls/Zs Silver 13 Kg ",
         "Brand": "SAMSUNG",
         "activePrice": "319.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Ble Class=tablapdp ID=tablapdp": "Class=tablapdp ID=tablapdp"
         }
      },
      "20370241": {
         "familia": "AUDIO",
         "sku": 20370241,
         "Name": "Barra De Sonido Sc Ua3Pu K All In One ",
         "Brand": "PANASONIC",
         "activePrice": "129.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20370893": {
         "familia": "ELECTRO HOGAR",
         "sku": 20370893,
         "Name": "Refrigerador 231Lts Nordik 415 Plus Inox ",
         "Brand": "MADEMSA",
         "activePrice": "169.990",
         "CMRprice": "159.990",
         "strikeOutPrice": "204.990",
         "tableData": {
            "Marca": "Mademsa",
            "Modelo": "Combi Nordik 415 Plus",
            "Color": "Inox",
            "Capacidad Total": "231 Litros",
            "Capacidad Refrigerador": "141 Lt",
            "Capacidad Freeezer": "90 Lt",
            "Material de Bandejas": "Vidrio Templado de Alta Resistencia",
            "Sistema Antiadherente ": "En Compartimiento Superior ",
            "Gavetas": "2 Cajones",
            "Freezer": "4 Cajones",
            "Peso": "60 KG",
            "Alto ": "165 CMS",
            "Ancho ": "55 CMS",
            "Profundidad": "58 CMS",
            "Consumo": "25,2 Kwh/mes",
            "Frio Directo": "Si"
         }
      },
      "20376225": {
         "familia": "ELECTRO HOGAR",
         "sku": 20376225,
         "Name": "Lavadora 13Kg Wt13Wsb Bco Lg. ",
         "Brand": "LG",
         "activePrice": "319.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20376226": {
         "familia": "ELECTRO HOGAR",
         "sku": 20376226,
         "Name": "Refrigerador 209Lt Lt23Bpp Plati/Silver ",
         "Brand": "LG",
         "activePrice": "309.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20376227": {
         "familia": "ELECTRO HOGAR",
         "sku": 20376227,
         "Name": "Refrigerador 254Lt Lt29Bppx Silver ",
         "Brand": "LG",
         "activePrice": "359.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20377536": {
         "familia": "ELECTRO HOGAR",
         "sku": 20377536,
         "Name": "Refrigerador 278Lt Rfd 377S Silv ",
         "Brand": "DAEWOO",
         "activePrice": "229.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20392649": {
         "familia": "ELECTRO HOGAR",
         "sku": 20392649,
         "Name": "Cocina 4 Quemadores 755S Silver  ",
         "Brand": "MADEMSA",
         "activePrice": "169.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Descripción": "Cocina 4 Quemadores 755s Silver",
            "Marca": "Mademsa",
            "Clasificación Energética": "A",
            "Luz En Horno ": "Si ",
            "Cubierta ": "Acero Inoxidable",
            "Parrillas Esmaltadas": "2",
            "Quemadores Rapidos": "2",
            "Quemadores Semi Rapidos": "2",
            "Tapa": "Cubierta de Cristal Templado",
            "Puerta Del Horno ": "Cristal Templado",
            "Bandeja ": "Enlozada En Horno Con Parrilla",
            "Quemadores Laterales Móviles ": "En Horno ",
            "Vólumen Del Horno ": "66 Litros ",
            "Color ": "Silver",
            "Peso ": "39 KG",
            "Alto ": "85 CMS",
            "Ancho ": "56 CMS",
            "Fondo": "58 CMS"
         }
      },
      "20398519": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20398519,
         "Name": "Smart Tv 40\" Fhd Mgs4005X ",
         "Brand": "MASTER G",
         "activePrice": "179.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Descripción": "Smart TV 40\" Fhd ",
            "Marca": "Master-G",
            "Modelo": "Mgs4005x",
            "Tecnología ": "Smart TV",
            "Resolución": "Full HD",
            "Tamaño de Pantalla ": "40 Pulgadas",
            "Entradas HDMI": "2",
            "Entradas USB ": "1",
            "Potencia de los Parlantes ": "20 Watts",
            "Ancho ": "90,6 CMS",
            "Alto Con Base": "58,95 CMS",
            "Profundidad Con Base": "21,19 CMS",
            "Peso": "7 KG",
            "Garantía": "1 Año "
         }
      },
      "20402734": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20402734,
         "Name": "Smart Tv 49\" Uhd 4K Rled L49D1202Uhd ",
         "Brand": "RECCO",
         "activePrice": "189.990",
         "CMRprice": null,
         "strikeOutPrice": "319.990",
         "tableData": {
            "Modelo": " Rled-L49d1620smt",
            "Color": "Negro",
            "Alto": "(Con/sin Base): 69,3/64,4 Cm",
            "Ancho": "110,8 Cm",
            "Profundidad": "(Con/sin Base): 23,9/9,4 Cm",
            "Entrada USB": "2",
            "Entrada HDMI": "3",
            "Peso": "10,5 KG",
            "Tamaño de La Pantalla": "49 Pulgadas",
            "Resolucion": "Full HD",
            "Tecnologia ": "LED",
            "Smart TV": "Si"
         }
      },
      "20402944": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20402944,
         "Name": "Smart Tv 32\" Hd 32Lk610Bpsa.Awh ",
         "Brand": "LG",
         "activePrice": "199.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Descripción": "Smart TV 32\" HD 32lk610bpsa.awh Lg ",
            "Marca": "Lg",
            "Tamaño de Pantalla": "32\"",
            "Resolución": "HD",
            "Tecnología": "LED",
            "Conexión Bluetooth": "No",
            "Entradas USB": "1",
            "Smart TV": "Si",
            "Entradas HDMI": "3",
            "Control Remoto Incluido": "Si",
            "Tipo": "Televisores",
            "Entradas Vga": "Sin Entradas",
            "Entradas Rca": "1",
            "Entradas Auxiliares 3.5 MM": "No Incluye",
            "Entrada Internet": "Si",
            "Conexión Wifi": "Si",
            "Potencia de los Parlantes": "10 W",
            "Peso": "5,15 KG",
            "Ancho": "74,2 Cm",
            "Alto": "47,2 Cm",
            "Alto Sin Base": "47,2 Cm",
            "Lentes 3d Incluidos": "No",
            "Garantia": "1 Año",
            "Tipo de Pantalla": "Plana",
            "Hecho En ": "No Aplica"
         }
      },
      "20402945": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20402945,
         "Name": "Smart Tv 43\" Fhd 43Lk5700 ",
         "Brand": "LG",
         "activePrice": "279.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Descripción": "Smart TV 43\" Fhd Lk5700 Lg",
            "Marca": "Lg",
            "Modelo ": "43lk5700",
            "Tipo ": "LED",
            "Resolución": "Full HD",
            "Tamaño de La Pantalla ": "43 Pulgadas",
            "Smart TV": "Si",
            "Conexión de Bluetooth": "Si",
            "Entradas HDMI": "2",
            "Procesador": "Quad Core",
            "Sistema Operativo": "Webos",
            "Entradas USB": "1",
            "Potencia de los Parlantes ": "10 W",
            "Alto": " 61,5 Cm",
            "Profundidad": "18,7",
            "Ancho": "97,7",
            "Peso": "8kg",
            "Garantia": "12 Meses Con El Proveedor"
         }
      },
      "20402949": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20402949,
         "Name": "Smart Tv 55\" Uhd 55Uk6350 ",
         "Brand": "LG",
         "activePrice": "449.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Descripción": "Smart TV 55' UHD 55uk6350 Lg",
            "Marca": "Lg",
            "Tamaño de Pantalla": "55\"",
            "Resolución": "4k Ultra HD",
            "Tecnología": "LED",
            "Conexión Bluetooth": "Si",
            "Entradas USB": "2",
            "Smart TV": "Si",
            "Entradas HDMI": "3",
            "Control Remoto Incluido": "Si",
            "Tipo": "Televisores",
            "Entradas Vga": "Sin Entradas",
            "Entradas Rca": "1",
            "Entradas Auxiliares 3.5 MM": "No Incluye",
            "Entrada Internet": "Si",
            "Conexión Wifi": "Si",
            "Potencia de los Parlantes": "20 W",
            "Peso": "15 KG",
            "Ancho": "124, 7 Cm",
            "Alto": "78,5 Cm",
            "Alto Sin Base": "78,5 Cm",
            "Lentes 3d Incluidos": "No",
            "Garantia": "1 Año",
            "Tipo de Pantalla": "Plana",
            "Hecho En ": "No Aplica"
         }
      },
      "20403216": {
         "familia": "AUDIO",
         "sku": 20403216,
         "Name": "Parlante Karaoke Bt Black Noise ",
         "Brand": "MASTER-G",
         "activePrice": "59.990",
         "CMRprice": null,
         "strikeOutPrice": "99.990",
         "tableData": {
            "Ean": "7808748510156",
            "Modelo": "Black Noise",
            "Medidas": " 170 X 835 X 335 MM ",
            "Peso": "8,3 KG",
            "Pantalla Display LED": "Si",
            "Iluminacion": "LED Rgb",
            "Fashing ": "Desing",
            "Woofer": "5,25\" X2 / Tweeter: 2,5\" X2",
            "Potencia Maxima": "90w Rms",
            "Rejilla": "De Acero",
            "Conectivdad Bluetooth": "Si",
            "Funcion Karaoke": "Si",
            "Reproductor Mp3": "Si",
            "Radio Fm": "Si",
            "Control de Volumen Y Eco Para Mocrofono": "Si",
            "Control de Tonos": "Control Remoto ",
            "Puerto USB": "Si",
            "Entradas Auxiliares": "2",
            "Entradas de Microfono": "2",
            "Incluye": "Control Remoto"
         }
      },
      "20404619": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20404619,
         "Name": "Smart Tv 55 Uhd Un55Nu7100Gxzs ",
         "Brand": "SAMSUNG",
         "activePrice": "299.990",
         "CMRprice": null,
         "strikeOutPrice": "429.990",
         "tableData": {
            "Modelo": "Un55nu7100gxzs",
            "Garantía": "1 Año",
            "Tipo": "Smart TV",
            "Conexión": "Wifi, USB",
            "Entradas": "HDMI",
            "Tecnologia": "LED",
            "Resolucion": "4k Ultra HD",
            "Tamaño de Pantalla": "55\"",
            "Origen": "Mexico",
            "Alto": "79,3 Cm",
            "Ancho": "123,9 Cm",
            "Profundidad": "26,1/5,9 Cm",
            "Peso": "17,7 KG",
            "Color": "Negro",
            "Procesador": "Quad Core"
         }
      },
      "20404889": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20404889,
         "Name": "Smart Tv 50\" Uhd Un50Nu7100Gxzs ",
         "Brand": "SAMSUNG",
         "activePrice": "259.990",
         "CMRprice": null,
         "strikeOutPrice": "349.990",
         "tableData": {
            "Modelo": "Un50nu7100gxzs",
            "Garantía": "1 Año",
            "Tipo": "Smart TV",
            "Conexión": "Wifi, USB",
            "Entradas": "HDMI",
            "Tecnologia": "LED",
            "Resolucion": "4k Ultra HD",
            "Tamaño de Pantalla": "50\" ",
            "Origen": "Mexico",
            "Alto": "72,88 Cm",
            "Ancho": "112,48 Cm",
            "Profundidad": "26,13/5,97 Cm",
            "Peso": "14,4 KG",
            "Color": "Negro",
            "Procesador": "Quad Core"
         }
      },
      "20405932": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20405932,
         "Name": "Smart Tv 49\" Uhd 4K 49Uk6200 ",
         "Brand": "LG",
         "activePrice": "349.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Descripción": "Smart TV 49\" UHD 4k 49uk6200 Lg ",
            "Marca": "Lg",
            "Tipo de Pantalla": "LCD LED",
            "Pantalla": "49\"",
            "Resolución": "3840*2160",
            "Bluetooth Audio Playback": "Si",
            "Potencia": "20w",
            "Lg Sound Sync / Bluetooth": "Si",
            "Procesador": "Quad Core",
            "Conectividad": "HDMI, USB, Lan, Wifi, Bluetooth",
            "Color": "Negro",
            "Base": "2 Patas",
            "Peso": "14,2 KG",
            "Control Remoto": "Si",
            "Manual ": "Si"
         }
      },
      "20406720": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20406720,
         "Name": "Smart Tv Tcl 49\" Fhd Curvo ",
         "Brand": "TCL",
         "activePrice": "399.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Marca": "TCL",
            "Color": "Gris",
            "Pantalla": "49\"",
            "Resolución": "Full HD",
            "Pixeles": "1920 X 1080",
            "Potencia": "110 W",
            "Sintonizador Digital": "Si",
            "Tecnología": "Smart TV",
            "Puertos": "HDMI, USB 2.0/3.0",
            "Conectividad": "Wi-Fi / Ethernet",
            "Ancho": "109.5 Cm",
            "Alto": "69 Cm",
            "Profundidad": "24 Cm",
            "Peso": "14.5 Cm"
         }
      },
      "20410075": {
         "familia": "CELULARES",
         "sku": 20410075,
         "Name": "Smartphone Galaxy J2 Core Negro Movistar ",
         "Brand": "MOVISTAR",
         "activePrice": "69.990",
         "CMRprice": "59.990",
         "strikeOutPrice": "79.990",
         "tableData": {
            "Modelo": "-",
            "Garantía": "1 Año",
            "Color": "Negro",
            "Procesador": "Quad-Core 1.4 Ghz",
            "Tipo de Producto": "Celulares Smartphones",
            "Memoria Expansible": "Hasta 64 Gb",
            "Cámara Trasera": "8 Mp",
            "Camara Frontal": "5 Mp",
            "Memoria Ram": "1.5 Gb",
            "Compañía": "Movistar",
            "Sistema Operativo": "Android 7.0 Nougat",
            "Bandas Internet": "Gsm Gsm850 Gsm900 Dcs1800 Pcs1900 / 3g B1(2100) B2(1900) B4(aws) B5(850) B8(900) / 4g-Lte: B4(aws) B7 (2600) B28(700)"
         }
      },
      "20411497": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20411497,
         "Name": "Smart Tv 43\" Fhd Lt 43Kb385 ",
         "Brand": "JVC",
         "activePrice": "159.990",
         "CMRprice": "149.990",
         "strikeOutPrice": "279.990",
         "tableData": {

         }
      },
      "20411830": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20411830,
         "Name": "Smart Tv 55\" L55D1202Smt Rled ",
         "Brand": "RECCO",
         "activePrice": "219.990",
         "CMRprice": null,
         "strikeOutPrice": "249.990",
         "tableData": {
            "Descripción": "Smart TV 55\" Rled-L55d1202smt Recco",
            "Marca": "Recco",
            "Tamaño de La Pantalla": "55 Pulgadas",
            "Resolución ": "Full HD",
            "Tecnología ": "LCD",
            "Bluetooth Audio Playback": "No",
            "Entradas USB": "2",
            "Smart TV": "Si",
            "Entradas HDMI": "3",
            "Control Remoto ": "Si",
            "Modelo ": "Rled-L55d1202sm",
            "Potencia de los Parlantes": "2×8 W",
            "Peso": "13,8 KG",
            "Profundidad": "24.8 Cm",
            "Ancho": "124.9 CMS"
         }
      },
      "20412031": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20412031,
         "Name": "Smart Tv 50\" Uhd 4K Mgu5020X ",
         "Brand": "MASTER G",
         "activePrice": "299.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20415910": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20415910,
         "Name": "Smart Tv Hd 32\" Un32J4290Agxzs ",
         "Brand": "SAMSUNG",
         "activePrice": "149.990",
         "CMRprice": "139.990",
         "strikeOutPrice": "179.990",
         "tableData": {
            "Descripción": "Smart TV 32 ¿",
            "Marca": "Samsung",
            "Modelo": "Un32j4290agxzs",
            "Tamaño de La Pantalla": "32\"",
            "Resolución": "HD",
            "Tecnología": "LCD/smart TV",
            "Entradas HDMI": "2",
            "Entradas USB ": "1",
            "Control Remoto Incluido ": "Si",
            "Ancho ": "73,74 CMS",
            "Alto Con/sin Base": "46,54/43,8 CMS",
            "Profundidad Con/sin Base": "15,05/7,41 CMS",
            "Peso": "4,1 KG",
            "Garantía": "12 Meses Con El Proveedor "
         }
      },
      "20416071": {
         "familia": "ELECTRO HOGAR",
         "sku": 20416071,
         "Name": "Refrigerador Samsung Side By Side 535Lt  ",
         "Brand": "Samsung",
         "activePrice": " 379.990",
         "CMRprice": "359.990",
         "strikeOutPrice": "699.990",
         "tableData": {
            "Fabrica": "SSEC",
            "Cantidad de Carga": "36",
            "Nr Certificado Seguridad": "E-013-04-9468",
            "NR Certificado Eficiencia": "E-013-04-9479",
            "Código QR": "252532",
            "Eficiencia y Consumo": "A+ [36,40]",
            "Origen": "China",
            "Color": "Easy Clean Steel",
            "Capacidad Total Neta": "535 lt",
            "Capacidad Neta Congelador": "179 lt",
            "Capacidad Neta Refrigeración": "356 lt",
            "Medidas Netas con Manillas/Visagras [cm]": "91,2 cm Ancho x 178,9 cm Alto x 73,4 cm prof",
            "Medidas Brutas [cm]": "97,4 cm Ancho x 190,7 cm Alto x 78,9 cm prof",
            "Medidas Netas sin Manillas/Visagras [cm]": "91,2 cm Ancho x 178,9 cm Alto x 67,2 cm prof",
            "Peso Neto": "98 kg",
            "Peso Bruto": "105 kg",
            "Tensión Nominal / Frecuencia": "220V~240V / 50Hz",
            "Corriente Nominal": "2A",
            "Garantía Producto": "1 año",
            "Garantía del Compresor": "10 años",
            "Grados de Enfriamiento": "4 Estrellas",
            "Refrigerante": "R600a",
            "Incluye Instalación": "No requiere instalacion",
            "Dispensador de Agua Automático": "NO",
            "Dispensador de Hielo ": "NO",
            "Tipo de Ice Maker": "Cubetera",
            "Tipo de Compresor ": "Digital Inverter",
            "Función Power Cool": "SI",
            "Función Power Freeze": "SI",
            "Filtro de Agua": "NO",
            "Control de Temperatura": "Panel LED Digital",
            "Tipo de Manillas": "Manillas",
            "Nr.Bandejas Congelador": "4",
            "Nr. Gavetas Puerta Congelador": "5",
            "Nr. Cajones Congelador": "2",
            "Nr. Bandejas Refrigeración": "5",
            "Nr. Gavetas Puerta Refrigeración": "4",
            "Gavetas de Puerta Ancha": "SI",
            "Nr. Cajones Refrigerador": "2",
            "BandejaHuevos": "SI",
            "Rack para Vinos": "NO",
            "Iluminación ": "LED",
            "Material Bandejas": "Vidrio Templado",
            "Patas Ajustables": "SI",
            "Ruedas de Desplazamiento": "SI",
            "Bloqueo de Panel para niños": "SI",
            "Alarma de Puerta": "SI"

         }
      },
      "20416073": {
         "familia": "ELECTRO HOGAR",
         "sku": 20416073,
         "Name": "Lavadora 17 Kg Wa17F7L6Ddw/Zs Blanco ",
         "Brand": "SAMSUNG",
         "activePrice": "259.990",
         "CMRprice": null,
         "strikeOutPrice": "379.990",
         "tableData": {
            "Modelo": "Wa17f7l6ddw/zs",
            "Capacidad de Carga": "16 KG",
            "Tipo de Carga": "Superior",
            "Material de Tambor": "Acero Inoxidable",
            "Tecnología": "Inverter",
            "Número de Programas": "12",
            "Tipo de Pantalla": "Digital",
            "Potencia": "600w",
            "Eficiencia Energética": "No Aplica",
            "Tipo de Lavadora": "Automática",
            "Ajuste de Temperatura": "Si",
            "Centrifugado": "Si",
            "Peso": "48 KG",
            "Alto": "103 Cm",
            "Ancho": "63 Cm",
            "Profundidad": "67 Cm",
            "Número de Certificación": "E-013-03-12565",
            "Hecho En": "México"
         }
      },
      "20416504": {
         "familia": "ELECTRO HOGAR",
         "sku": 20416504,
         "Name": "Refrigerador 342Lt Altus 1350 Inox ",
         "Brand": "MADEMSA",
         "activePrice": "269.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20416505": {
         "familia": "ELECTRO HOGAR",
         "sku": 20416505,
         "Name": "Lavadora 11,5 Kg  Efficacce Inox ",
         "Brand": "MADEMSA",
         "activePrice": "119.990",
         "CMRprice": null,
         "strikeOutPrice": "189.990",
         "tableData": {
            "Descripción": "Lavadora 11,5kg Efficacce Inox Mademsa",
            "Marca": "Mademsa",
            "Capacidad": "11,5 KG",
            "Consumo de Agua Fría": "110 Lts/ciclo",
            "Programas de Lavado": "6",
            "Niveles de Agua": "4",
            "Color": "Blanco",
            "Alto ": "96,5 CMS",
            "Ancho": "53,5 CMS",
            "Profundidad": "57,0 CMS",
            "Peso": "30,5 KG",
            "Tambor": "Acero Inoxidable"
         }
      },
      "20416506": {
         "familia": "ELECTRO HOGAR",
         "sku": 20416506,
         "Name": "Lavadora Mademsa 15,5Kg Efficacce Szg Inox ",
         "Brand": "MADEMSA",
         "activePrice": "169.990",
         "CMRprice": "159.990",
         "strikeOutPrice": "229.990",
         "tableData": {
            "Descripción": "Lavadora 15,5kg Efficacce Szg Inox Mademsa ",
            "Marca": "Mademsa",
            "Capacidad": "15,5 KG",
            "Programas de Lavado ": "6",
            "Niveles de Agua ": "6",
            "Color": "Silver",
            "Alto": "1.040 MM",
            "Ancho": "610 MM",
            "Profundidad": "665 MM",
            "Peso Neto ": "39 KG",
            "Consumo de Agua Fría ": "167 Litros/ciclo"
         }
      },
      "20416887": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20416887,
         "Name": "Smart Tv 40\" Line Android Fhd ",
         "Brand": "TCL",
         "activePrice": "169.990",
         "CMRprice": null,
         "strikeOutPrice": "259.990",
         "tableData": {
            "Producto": "Smart TV Line Android Fhd 40s6500 TCL",
            "Marca": "TCL",
            "Modelo": "40s6500",
            "Garantia": "1 Año",
            "Tecnologia": "LED Full HD",
            "Pantalla": "40\"",
            "Sistema Operativo": "Android TV",
            "Tecnologia ": "Hdr",
            "Conexión": "Wifi, Bluetooth, USB, 2 HDMI"
         }
      },
      "20419482": {
         "familia": "AUDIO",
         "sku": 20419482,
         "Name": "Parlante Bt Xb10/Red ",
         "Brand": "SONY",
         "activePrice": "39.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20419483": {
         "familia": "AUDIO",
         "sku": 20419483,
         "Name": "Parlante Bt Xb10/Black ",
         "Brand": "SONY",
         "activePrice": "39.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20419890": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20419890,
         "Name": "Smart Tv Tcl 65\" Uhd 4K ",
         "Brand": "TCL",
         "activePrice": "599.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Descripción": "Smart TV 65\" UHD TCL",
            "Marca": "TCL",
            "Modelo ": "P65us",
            "Pantalla": "65\"",
            "Resolución": "3840*2160",
            "Internet": "Cable/inalambrico",
            "Salida de Audio ": "2x8w",
            "Color": "Negro",
            "Control Remoto": "Si ",
            "Manual Del Propietario": "Si",
            "USB": "Si",
            "HDMI": "Si",
            "Contraste": "5001   1 "
         }
      },
      "20420011": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20420011,
         "Name": "Smart TV 43\" 4K UHD MGU4330X ",
         "Brand": "MASTER G",
         "activePrice": "199.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Descripción": "Smart TV 43\" 4k UHD Mgu5020x Master-G",
            "Marca": "Master G",
            "Pantalla": "43 Pulgadas",
            "Resolución": "HD",
            "Función": "Smart TV",
            "Wifi": "Integrado",
            "USB": "2",
            "HDMI": "3",
            "Potencia de los Parlantes": "16 Watts",
            "Control Remoto": "Si  ",
            "Peso": "6,9 KG",
            "Ancho ": "97,1 CMS",
            "Alto": "60,7 CMS",
            "Profundidad": "21,3 CMS"
         }
      },
      "20420012": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20420012,
         "Name": "Smart Tv Led 65\" Uhd 4K 65Uk6350Psc.Awh ",
         "Brand": "LG",
         "activePrice": "449.990",
         "CMRprice": null,
         "strikeOutPrice": "899.990",
         "tableData": {
            "Descripción": "Smart TV LED 65\" 65uk6350psc.awh Ud Lg",
            "Marca": "Lg",
            "Tamaño de La Pantalla": "65 Pulgadas",
            "Resolucion ": "4k Ultra HD",
            "Tipo": "LED",
            "Entrada USB": "2",
            "Entradas HDMI": "3",
            "Conexión A Bluetooth": "Si",
            "Control Remoto Incluído ": "Si",
            "Procesador ": "Quad Core",
            "Sintonizador Digital ": "Si ",
            "Potencia de los Parlantes ": "20 Watts",
            "Peso": "22,1 KG",
            "Ancho ": "146,8 CMS",
            "Alto Con/sin Base": "91,1 CMS",
            "Garantía ": "12 Meses Con El Proveedor "
         }
      },
      "20420014": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20420014,
         "Name": "Smart Tv Led 75\" Uhd 4K 75Uk6570Psa.Awh Ud ",
         "Brand": "LG",
         "activePrice": "899.990",
         "CMRprice": null,
         "strikeOutPrice": "1.999.990",
         "tableData": {
            "Tamaño de La Pantalla": "75 Pulgadas",
            "Smart TV": "Si",
            "Potencia de los Parlantes ": "20 Watts",
            "Entrada USB": "2",
            "Entradas HDMI": "3",
            "Definición ": "UHD",
            "Peso": "34,6 KG",
            "Garantía": "12 Meses Con El Proveedor "
         }
      },
      "20420015": {
         "familia": "AUDIO",
         "sku": 20420015,
         "Name": "Speaker Vintage Wood M10 ",
         "Brand": "URBANO",
         "activePrice": "59.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Marca": "Urbano Vintage",
            "Tipo": "Audio - Outdoor",
            "Potencia": "35w",
            "Características": "Bluetooth",
            "Resistente  ": "Resistente A La Humedad"
         }
      },
      "20420016": {
         "familia": "AUDIO",
         "sku": 20420016,
         "Name": "Speaker Vintage M20 Gray ",
         "Brand": "URBANO",
         "activePrice": "69.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Marca": "Urbano Vintage",
            "Tipo": "Audio - Outdoor",
            "Modelo": "Ud-Spm202",
            "Potencia Rms": "5w X 2",
            "Potencia": "10w",
            "Ancho": "13 Cm",
            "Alto ": "21 Cm",
            "Profundidad": "10 Cm",
            "Entradas USB": "1",
            "Características": "Bluetooth/lector Sd-Micro Sd",
            "Accesorios": "Cable de Carga Y Manual Del Usuario",
            "Material ": "Acabado de Bambu",
            "Garantia": "3 Meses Con El Proveedor ",
            "Hecho En ": "China"
         }
      },
      "20420017": {
         "familia": "AUDIO",
         "sku": 20420017,
         "Name": "Speaker Vintage M20 Black ",
         "Brand": "URBANO",
         "activePrice": "69.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Marca": "Urbano Vintage",
            "Tipo": "Audio - Outdoor",
            "Potencia": "30w",
            "Características": "Bluetooth",
            "Material ": "Acabado de Bambu"
         }
      },
      "20420018": {
         "familia": "AUDIO",
         "sku": 20420018,
         "Name": "(D)Parlante Speaker Aluminium Black C29 ",
         "Brand": "URBANO",
         "activePrice": "29.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20420019": {
         "familia": "AUDIO",
         "sku": 20420019,
         "Name": "Parlante Speaker Aluminium Blue C29 ",
         "Brand": "URBANO",
         "activePrice": "29.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20420020": {
         "familia": "AUDIO",
         "sku": 20420020,
         "Name": "Speaker Vintage Double Bluetooth T10 ",
         "Brand": "URBANO",
         "activePrice": "69.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Marca": "Urbano Vintage",
            "Tipo": "Audio - Outdoor",
            "Potencia": "35w",
            "Características": "Compatible Con Laptos/tablets/smartphones/media Players/ Gamesconsoles",
            "Bateria ": "Recargable",
            "Resistente  ": "A La Humedad"
         }
      },
      "20420021": {
         "familia": "AUDIO",
         "sku": 20420021,
         "Name": "Parlante Superbass Waterproof X26 Kit ",
         "Brand": "URBANO",
         "activePrice": "49.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20420022": {
         "familia": "AUDIO",
         "sku": 20420022,
         "Name": "Parlante Superbass Waterproof X26 ",
         "Brand": "URBANO",
         "activePrice": "7.990",
         "CMRprice": null,
         "strikeOutPrice": "29.990",
         "tableData": {

         }
      },
      "20420023": {
         "familia": "AUDIO",
         "sku": 20420023,
         "Name": "Parlante Hifi Speaker Black M246 ",
         "Brand": "URBANO",
         "activePrice": "69.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20420661": {
         "familia": "AUDIO",
         "sku": 20420661,
         "Name": "Parlante Karaoke 250W ",
         "Brand": "PANASONIC",
         "activePrice": "59.990",
         "CMRprice": null,
         "strikeOutPrice": "89.990",
         "tableData": {
            "Descripción": "Parlante Karaoke 250w Panasonic  Sc-Cmax4pu-K",
            "Marca": "Panasonic",
            "Entradas Para Micrófono ": "2",
            "Funciones": "Cancelación de Voz, Control de Tono, Control de Compás, Control de Nivel Bgm.",
            "Ecualizador de Micrófono ": "Si",
            "Potencia": "250 Watts",
            "App": "Max Jukebox",
            "Efectos ": "6 Efectos de Karaoke",
            "Woofer ": "20 Cm",
            "USB": "Si",
            "Bluetooth": "Corriente Alterna"
         }
      },
      "20420969": {
         "familia": "AUDIO",
         "sku": 20420969,
         "Name": "Parlante Ultra Revolution ",
         "Brand": "MASTER G",
         "activePrice": "69.990",
         "CMRprice": "59.990",
         "strikeOutPrice": "79.990",
         "tableData": {

         }
      },
      "20420972": {
         "familia": "ELECTRO HOGAR",
         "sku": 20420972,
         "Name": "Refrigerador 235L No Frost Gt26Bgp Silver ",
         "Brand": "LG",
         "activePrice": "249.990",
         "CMRprice": null,
         "strikeOutPrice": "319.990",
         "tableData": {
            "Modelo ": "Gt26bpg",
            "Tipo ": "Top Mount",
            "Color": "Grafito",
            "Capacidad": "235 Lt",
            "Iluminación ": "LED",
            "Material de Bandejas ": "Vidrio Templado ",
            "Alto ": "157 CMS",
            "Ancho": "55,5 CMS",
            "Profundidad": "62 CMS",
            "Peso": "49 CMS",
            "Sistema de Frío ": "No Frost ",
            "Dispensador de Hielo ": "No  ",
            "Dispensador de Agua ": "No ",
            "Garantía ": "1 Año ",
            "Garantía Del Motor": "10 Años "
         }
      },
      "20421195": {
         "familia": "AUDIO",
         "sku": 20421195,
         "Name": "Parlante Karaoke Bt Mandala Green E ",
         "Brand": "GREEN.E",
         "activePrice": "14.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20421399": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20421399,
         "Name": "Smart Tv Samsung 65\" Uhd ",
         "Brand": "SAMSUNG",
         "activePrice": "499.990",
         "CMRprice": null,
         "strikeOutPrice": "679.990",
         "tableData": {
            "Modelo": "Smart TV 65\" UHD 4k Nu7100 Samsung",
            "Marca": "Samsung",
            "Resolución": "4k UHD 65\" 3840*2160",
            "Tipo": "Smart TV",
            "Conexiones": "3 HDMI /2 USB",
            "Voltaje": "20w"
         }
      },
      "20422244": {
         "familia": "ELECTRO HOGAR",
         "sku": 20422244,
         "Name": "Lavadora 13Kg Wt13Wpb Blanca ",
         "Brand": "LG",
         "activePrice": "219.990",
         "CMRprice": null,
         "strikeOutPrice": "299.990",
         "tableData": {
            "Modelo ": "Wt13wpb",
            "Tipo de Motor": "Smart Inverter",
            "Color": "Blanco",
            "Capacidad": "13 KG",
            "Tecnología Pulsador": "Turbo Drum ",
            "Garantía": "10 Años ",
            "Lavado Diferido ": "Si ",
            "Tipo de Panel ": "Digital ",
            "Alto ": "96 CMS",
            "Ancho ": "59 CMS",
            "Profundidad": "60,6 CMS",
            "Peso ": "40kg",
            "Potencia ": "200 Watts",
            "Material de Tapa ": "Vidrio "
         }
      },
      "20422298": {
         "familia": "AUDIO",
         "sku": 20422298,
         "Name": "Parlante Karaoke 8\" Street Voice Mlab ",
         "Brand": "MLAB",
         "activePrice": "29.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20422300": {
         "familia": "AUDIO",
         "sku": 20422300,
         "Name": "Parlante Bt Karaoke Mgultraextreme ",
         "Brand": "MASTER G",
         "activePrice": "129.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20422891": {
         "familia": "ELECTRO HOGAR",
         "sku": 20422891,
         "Name": "Lavadora Seca 9K/5K Wd90M4453Jw/Zs Bco ",
         "Brand": "SAMSUNG",
         "activePrice": "439.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20423020": {
         "familia": "ELECTRO HOGAR",
         "sku": 20423020,
         "Name": "Secadora Mademsa 7kg Solare 7100 ",
         "Brand": "MADEMSA",
         "activePrice": "159.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Descripción": "Secadora 7kg Solare 7100 Mademsa",
            "Marca": "Mademsa",
            "Capacidad": "7kg ",
            "Panel  ": "Digital de Uso Intuitivo",
            "Alto": "84 CMS",
            "Ancho": "59,5 CMS",
            "Fondo": "55 CMS",
            "Consumo de Energía": " 2,25 Kwh",
            "Niveles de Temperatura": "3",
            "Programas de Secado": "4",
            "Filtro Pelusas Removible": "Si"
         }
      },
      "20423021": {
         "familia": "ELECTRO HOGAR",
         "sku": 20423021,
         "Name": "Secadora 9Kg Solare 9100 ",
         "Brand": "MADEMSA",
         "activePrice": "179.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20424789": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20424789,
         "Name": "Smart Tv 32\" Hd Borderless Lt 32Kb197 ",
         "Brand": "JVC",
         "activePrice": "199.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20424790": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20424790,
         "Name": "Smart Tv 43\" Fhd Borderless Lt 43Kb397 ",
         "Brand": "JVC",
         "activePrice": "349.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20424791": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20424791,
         "Name": "Smart Tv 50\" Uhd 4K Lt 50Kb585 ",
         "Brand": "JVC",
         "activePrice": "199.990",
         "CMRprice": "189.990",
         "strikeOutPrice": "399.990",
         "tableData": {

         }
      },
      "20424792": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20424792,
         "Name": "Smart Tv 58\" Uhd 4K Lt 58Kb585 ",
         "Brand": "JVC",
         "activePrice": "299.990",
         "CMRprice": "289.990",
         "strikeOutPrice": "499.990",
         "tableData": {

         }
      },
      "20424795": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20424795,
         "Name": "Smart Tv 65\" Uhd 4K Lt 65Kc595 ",
         "Brand": "JVC",
         "activePrice": "599.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20425030": {
         "familia": "ELECTRO HOGAR",
         "sku": 20425030,
         "Name": "(D)Refrigerador 252Lts Lrb 280Nfi Inox ",
         "Brand": "LIBERO",
         "activePrice": "239.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20426138": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20426138,
         "Name": "Smart TV 43\" FHD J5202 ",
         "Brand": "SAMSUNG",
         "activePrice": "189.990",
         "CMRprice": "179.990",
         "strikeOutPrice": "249.990",
         "tableData": {
            "Descripción": "Smart TV  43¿ Fhd   ",
            "Marca": "Samsung",
            "Modelo": "Un43j5202agxzs",
            "Tamaño de La Pantalla": "43\"",
            "Resolución": "Full HD",
            "Tecnología": "Smart TV",
            "Entradas HDMI": "2",
            "Entradas USB ": "1",
            "Control Remoto Incluido ": "Si",
            "Ancho ": "96,7 CMS",
            "Alto Con/sin Base": "61,9/57,1 CMS ",
            "Profundidad Con/sin Base": "22,6/8 CMS",
            "Peso": "7,4 CMS",
            "Garantía": "12 Meses Con El Proveedor "
         }
      },
      "20426595": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20426595,
         "Name": "Smart TV 50\" UHD 4K 7095 ",
         "Brand": "SAMSUNG",
         "activePrice": "259.990",
         "CMRprice": "249.990",
         "strikeOutPrice": "349.990",
         "tableData": {
            "Marca": "Samsung",
            "Modelo": "Un50nu7095gxzs",
            "Tamaño de La Pantalla": "50\"",
            "Resolución": "4k Ultra HD",
            "Tecnología": "Smart TV",
            "Procesador": "Quad Core",
            "Control Remoto Incluido ": "Si",
            "Potencia de los Parlantes ": "20 Watts",
            "Ancho ": "112,2 CMS",
            "Alto Con/sin Base": "69,3 CMS / 64,9 CMS",
            "Profundidad": "24 CMS",
            "Peso": "9,5 KG",
            "Wifi": "Si",
            "Garantía": "12 Meses Con El Proveedor "
         }
      },
      "20427483": {
         "familia": "ELECTRO HOGAR",
         "sku": 20427483,
         "Name": "Lavadora 8Kg WA80H4200SW/ZS ",
         "Brand": "SAMSUNG",
         "activePrice": "169.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Marca": "Samsung",
            "Capacidad": "8 KG",
            "Color": "Blanco",
            "Pantalla": "Dual Cluster/inlay",
            "Air Turbo ": "Si",
            "Visor de Vidrio Templado ": "Si",
            "Pulsador ": "Wobble",
            "Niveles de Agua": "5 Niveles",
            "Tipo de Tambor": "Diamante ",
            "Bloqueo de Niños ": "Si ",
            "Cantidad de Ciclos": "6",
            "Peso ": "31 KG"
         }
      },
      "20427648": {
         "familia": "AUDIO",
         "sku": 20427648,
         "Name": "Audifono T450 Cable  Negro Jbl ",
         "Brand": "JBL",
         "activePrice": "9.990",
         "CMRprice": null,
         "strikeOutPrice": "29.990",
         "tableData": {

         }
      },
      "20427649": {
         "familia": "AUDIO",
         "sku": 20427649,
         "Name": "Audifono T450 Cable  Blanco Jbl ",
         "Brand": "JBL",
         "activePrice": "9.990",
         "CMRprice": null,
         "strikeOutPrice": "29.990",
         "tableData": {

         }
      },
      "20430693": {
         "familia": "CELULARES",
         "sku": 20430693,
         "Name": "Camara Deportiva Action Cam Fhd ",
         "Brand": "URBANO",
         "activePrice": "9.990",
         "CMRprice": null,
         "strikeOutPrice": "39.990",
         "tableData": {

         }
      },
      "20431150": {
         "familia": "CELULARES",
         "sku": 20431150,
         "Name": "Celular Senior Phone 3G Negro Irt ",
         "Brand": "IRT",
         "activePrice": "39.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20431776": {
         "familia": "CELULARES",
         "sku": 20431776,
         "Name": "Smartphone Mrd L03 Y6 2019 Black Claro ",
         "Brand": "HUAWEI",
         "activePrice": "89.990",
         "CMRprice": "79.990",
         "strikeOutPrice": "149.990",
         "tableData": {

         }
      },
      "20431777": {
         "familia": "CELULARES",
         "sku": 20431777,
         "Name": "Celular Senior Tiger 3026G Claro ",
         "Brand": "ALCATEL",
         "activePrice": "39.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         }
      },
      "20431779": {
         "familia": "CELULARES",
         "sku": 20431779,
         "Name": "Smartphone Pot L03 Psmart 2019 Blue Claro ",
         "Brand": "HUAWEI",
         "activePrice": "159.990",
         "CMRprice": "139.990",
         "strikeOutPrice": "199.990",
         "tableData": {

         }
      }
   }

   productDataStatic_OLD = {
      "20198063": {
         "familia": "ELECTRO HOGAR",
         "sku": 20198063,
         "Name": "Licuadora Minipimer Rmin-989W Inox ",
         "Brand": "Recco",
         "activePrice": "19.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Descripción": "Licuadora Manual Inox",
            "Marca": "Recco",
            "Modelo": "RMIN-989W",
            "Tecnología ": "Batidor de Helicóptero",
            "Resolución": "2",
            "Tamaño de Pantalla": "Acero Inoxidable",
            "Entradas HDMI": "400 Watts",
            "Entradas USB ": "Soporte para Muro. Vaso:  Capacidad Máxima 700 ML. Batidora Manual. Vaso con Tapa Y Cuchillos Desmontables"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20198063/20198063_0.jpg"
         ]
      },
      "20219387": {
         "familia": "ELECTRO HOGAR",
         "sku": 20219387,
         "Name": "Cocina 4 Platos ",
         "Brand": "Fensa",
         "activePrice": "164.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Tipo": "A gas",
            "Modelo": "F2525T",
            "Origen": "Chile",
            "Color": "Plateado",
            "Número de Quemadores": "4",
            "Capacidad Total ": "66 Lts",
            "Alto": "86,5 cm",
            "Ancho": "55 cm",
            "Profundidad": "56 cm",
            "Peso Neto": "40 Kg",
            "Garantía Producto": "1 año",
            "Material": "Acero Inoxidable",
            "Garantía del Proveedor": "12 meses",
            "Horno autolimpiate": "No",
            "Encendido electronico": "No",
            "Luz en el horno": "No"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20219387/20219387_0.jpg"
         ]
      },
      "20227267": {
         "familia": "ELECTRO HOGAR",
         "sku": 20227267,
         "Name": "Cocina 4 Platos F2235 ",
         "Brand": "Fensa",
         "activePrice": "129.990",
         "CMRprice": null,
         "strikeOutPrice": "139.990",
         "tableData": {
            "Descripción": "Cocina 4 Quemadores F 2235 BCA Fensa ",
            "Marca": "Fensa",
            "Modelo": "F 2235",
            "Color": "Blanco",
            "Capacidad ": "66 LTS",
            "Potencia": "Categoría II2 - 3 (GL Y/O GN)",
            "Ancho": "550 MM",
            "Alto": "865 MM",
            "Profundidad": "560 MM",
            "Peso": "33 KG",
            "Sistema": "Sistema de Seguridad Termopar en Horno"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20227267/20227267_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20227267/20227267_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20227267/20227267_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20227267/20227267_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20227267/20227267_4.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20227267/20227267_5.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20227267/20227267_6.jpg"
         ]
      },
      "20231088": {
         "familia": "ELECTRO HOGAR",
         "sku": 20231088,
         "Name": "Cocina 6 Quemadores Diva 820 Negro  ",
         "Brand": "Mademsa",
         "activePrice": "169.990",
         "CMRprice": null,
         "strikeOutPrice": "219.990",
         "tableData": {

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20231088/20231088_0.jpg"
         ]
      },
      "20256212": {
         "familia": "ELECTRO HOGAR",
         "sku": 20256212,
         "Name": "Lavadora 9 Kg Wa90H4400Sw/Zs Blanca  ",
         "Brand": "Samsung",
         "activePrice": "189.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Modelo": "WA90H4400SW/ZS",
            "Tipo": "Automática",
            "Origen": "Tailandia",
            "Certificación": "E-013-03-8660",
            "Alto": "88 CM",
            "Ancho": "52,5 CM",
            "Profundidad": "60 CM",
            "Peso": "31 Kg",
            "Material Tambor": "Acero Inoxidable",
            "Capacidad Carga": "9KG",
            "Potencia": "330W",
            "Color": "Blanco"
         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20256212/20256212_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20256212/20256212_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20256212/20256212_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20256212/20256212_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20256212/20256212_4.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20256212/20256212_5.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20256212/20256212_6.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20256212/20256212_7.jpg"
         ]
      },
      "20278337": {
         "familia": "ELECTRO HOGAR",
         "sku": 20278337,
         "Name": "Secadora 7Kg Reverplus 6470 Silver ",
         "Brand": "Fensa",
         "activePrice": "109.990",
         "CMRprice": "99.990",
         "strikeOutPrice": "199.990",
         "tableData": {

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20278337/20278337_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20278337/20278337_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20278337/20278337_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20278337/20278337_3.jpg"
         ]
      },
      "20297533": {
         "familia": "ELECTRO HOGAR",
         "sku": 20297533,
         "Name": "Freezer Horiz 142Lts Efc14A5Mnw Bco ",
         "Brand": "Electrolux",
         "activePrice": "139.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Tipo": "Freezers horizontales",
            "Modelo": "EFC14A5M",
            "Nr Certificado Seguridad": "E-013-03-10339",
            "NR Certificado Eficiencia": "E-013-01-70293",
            "Eficiencia y Consumo": "A+",
            "Origen": "China",
            "Color": "-",
            "Capacidad Total ": "142 Lt",
            "Alto": "85 cm",
            "Ancho": "73 cm",
            "profundida": "53,7 cm",
            "Peso Neto": "-",
            "Garantía Producto": "12 meses",
            "Material Bandejas": "Metal",
            "Ruedas de Desplazamiento": "si",
            "Alarma de Puerta": "NO",
            "Accesorio": "Incluye llaves y ruedas."

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20297533/20297533_0.jpg"
         ]
      },
      "20306742": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20306742,
         "Name": "Led 32\" Mglnx3290I ",
         "Brand": "Master-G",
         "activePrice": "79.990",
         "CMRprice": null,
         "strikeOutPrice": "159.990",
         "tableData": {
            "Modelo": "MGLNX3290I",
            "Tipo": "Básico",
            "Conexión": "USB",
            "Entradas": "HDMI",
            "Tecnología": "LED",
            "Resolución": "HD ",
            "Tamaño de Pantalla": "32\"",
            "Alto": "47 cm",
            "Ancho": "73.2 cm",
            "Profundidad": "17 cm",
            "Peso": "4,7 Kg",
            "Color": "Negro"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20306742/20306742_0.jpg"
         ]
      },
      "20334601": {
         "familia": "ELECTRO HOGAR",
         "sku": 20334601,
         "Name": "Sandwichera Rsa6150 Color Negro ",
         "Brand": "Recco",
         "activePrice": "9.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Marca": "Recco",
            "Modelo": "RSA-6150",
            "Potencia": "750W",
            "Capacidad": "2 panes",
            "Luz indicador encendido": "si",
            "Color": "Negro"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20334601/20334601_0.jpg"
         ]
      },
      "20340551": {
         "familia": "ELECTRO HOGAR",
         "sku": 20340551,
         "Name": "Hervidor 1.7 Lt Rhe  ",
         "Brand": "Recco",
         "activePrice": "9.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Marca": "Recco",
            "Modelo": "RHE-1.7P",
            "Base desmontable": "Si",
            "Filtro": "Si",
            "Tapa de Seguridad": "Si",
            "Peso": "0,89 kg",
            "Potencia": "2200W",
            "Capacidad": "1.7 lt",
            "Material": "Metal",
            "Indicador de temperatura": "Análogo",
            "Selector de temperatura": "si",
            "Panel de temperatura": "No",
            "Nivel de agua visible": "Si",
            "Luz indicador encendido": "Si"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20340551/20340551_0.jpg"
         ]
      },
      "20340560": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20340560,
         "Name": "Smart TV 32\" RLED-L32D1620SMT/L32D1200SMT ",
         "Brand": "Recco",
         "activePrice": "129.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20340560/20340560_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20340560/20340560_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20340560/20340560_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20340560/20340560_3.jpg"
         ]
      },
      "20359286": {
         "familia": "ELECTRO HOGAR",
         "sku": 20359286,
         "Name": "Lavadora Wa13J5730Ls/Zs Silver 13 Kg ",
         "Brand": "Samsung",
         "activePrice": "319.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20359286/20359286_0.jpg"
         ]
      },
      "20370893": {
         "familia": "ELECTRO HOGAR",
         "sku": 20370893,
         "Name": "Refrigerador 231Lts Nordik 415 Plus Inox ",
         "Brand": "Mademsa",
         "activePrice": "204.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Marca": "Mademsa",
            "Modelo": "Combi Nordik 415 Plus",
            "Color": "Inox",
            "Capacidad Total": "231 Litros",
            "Capacidad Refrigerados": "141 LT",
            "Capacidad Freezer": "90 LT",
            "Maretial de Bandejas": "Vidrio Templado de Alta Resistencia",
            "Sistema Antiadherente": "En Compartimiento Superior",
            "Gavetas": "2 Cajones",
            "Freezer": "4 Cajones",
            "Peso": "60 KG",
            "Alto ": "165 CMS",
            "Ancho ": "55 CMS",
            "Profundidad": "58 CMS",
            "Consumo": "25,2 KWH/MES",
            "Frío Directo": "SI"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20370893/20370893_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20370893/20370893_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20370893/20370893_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20370893/20370893_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20370893/20370893_4.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20370893/20370893_5.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20370893/20370893_6.jpg"
         ]
      },
      "20392649": {
         "familia": "ELECTRO HOGAR",
         "sku": 20392649,
         "Name": "Cocina 4 Quemadores 755S Silver  ",
         "Brand": "Mademsa",
         "activePrice": "169.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Descripción": "COCINA 4 QUEMADORES 755S SILVER",
            "Marca": "Mademsa",
            "Clasificación Energética": "A",
            "Luz en Horno": "SI ",
            "Cubierta": "Acero Inoxidable",
            "Parrillas Esmaltadas": "2",
            "Quemadores Rapidos": "2",
            "Quemadores Semi Rapidos": "2",
            "Tapa": "Cubierta Cristal Templado",
            "Puerta del Horno": "Cristal Templado",
            "Bandeja": "Enlozada en Hornocon Parrilla",
            "Quemadores Laterales Móviles": "en Horno",
            "Vólumen del Horno": "66 Litros ",
            "Color ": "Silver",
            "Peso ": "39 KG",
            "Alto ": "85 CMS",
            "Ancho ": "56 CMS",
            "Fondo": "58 CMS"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20392649/20392649_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20392649/20392649_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20392649/20392649_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20392649/20392649_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20392649/20392649_4.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20392649/20392649_5.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20392649/20392649_6.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20392649/20392649_7.jpg"
         ]
      },
      "20398519": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20398519,
         "Name": "Smart Tv 40\" Fhd Mgs4005X ",
         "Brand": "Master G",
         "activePrice": "149.990",
         "CMRprice": "139.990",
         "strikeOutPrice": "179.990",
         "tableData": {
            "Descripción": "Smart TV 40\" FHD ",
            "Marca": "Master-G",
            "Modelo": "MGS4005X",
            "Tecnología ": "Smart TV",
            "Resolución": "FULL HD",
            "Tamaño de Pantalla": "40 Pulgadas",
            "Entradas HDMI": "2",
            "Entradas USB ": "1",
            "Potencia Parlantes": "20 Watts",
            "Ancho ": "90,6 CMS",
            "Alto CON BASE": "58,95 CMS",
            "Profundidad CON BASE": "21,19 CMS",
            "Peso": "7 KG",
            "Garantía": "1 año "

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20398519/20398519_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20398519/20398519_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20398519/20398519_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20398519/20398519_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20398519/20398519_4.jpg"
         ]
      },
      "20402734": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20402734,
         "Name": "Smart Tv 49\" Uhd 4K Rled L49D1202Uhd ",
         "Brand": "Recco",
         "activePrice": "189.990",
         "CMRprice": null,
         "strikeOutPrice": "319.990",
         "tableData": {
            "Modelo": " Rled-L49D1620Smt",
            "Color": "Negro",
            "Alto": "(con/sin Base): 69,3/64,4 Cm",
            "Ancho": "110,8 Cm",
            "Profundidad": "(con/sin Base): 23,9/9,4 Cm",
            "Entrada Usb": "2",
            "Entrada Hdmi": "3",
            "Peso": "10,5 Kg",
            "Tamaño Pantalla": "49 Pulgadas",
            "Resolución": "Full Hd",
            "Tecnología ": "Led",
            "Smart Tv": "Si"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402734/20402734_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402734/20402734_1.jpg"
         ]
      },
      "20402944": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20402944,
         "Name": "Smart Tv 32\" Hd 32Lk610Bpsa.Awh ",
         "Brand": "LG",
         "activePrice": "159.990",
         "CMRprice": "149.990",
         "strikeOutPrice": "199.990",
         "tableData": {
            "Descripción": "Smart TV 32\" HD 32LK610BPSA.AWH LG ",
            "Marca": "LG",
            "Tamaño de Pantalla": "32\"",
            "Resolución": "HD",
            "Tecnología": "Led",
            "Conexión Bluetooth": "NO",
            "Entradas USB": "1",
            "Smart TV": "SI",
            "Entradas HDMI": "3",
            "Control Remoto Incluido": "SI",
            "Tipo": "Televisores",
            "Entradas VGA": "Sin Entradas",
            "Entradas RCA": "1",
            "Entradas Auxiliares 3.5 MM": "No Incluye",
            "Entrada Internet": "SI",
            "Conexión WiFi": "SI",
            "Potencia de los Parlantes": "10 W",
            "Peso": "5,15 KG",
            "Ancho": "74,2 CM",
            "Alto": "47,2 CM",
            "Alto sin Base": "47,2 CM",
            "Lentes 3D Incluidos": "NO",
            "Garantía": "1 año",
            "Tipo de Pantalla": "Plana",
            "Hecho en": "No aplica"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402944/20402944_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402944/20402944_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402944/20402944_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402944/20402944_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402944/20402944_4.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402944/20402944_5.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402944/20402944_6.jpg"
         ]
      },
      "20402945": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20402945,
         "Name": "Smart Tv 43\" Fhd 43Lk5700 ",
         "Brand": "LG",
         "activePrice": "279.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Descripción": "Smart TV 43\" FHD LK5700 LG",
            "Marca": "LG",
            "Modelo ": "43LK5700",
            "Tipo ": "Led",
            "Resolución": "FULL HD",
            "Tamaño Pantalla ": "43 PULGADAS",
            "Smart TV": "SI",
            "Conexión de Bluetooth": "SI",
            "Entradas HDMI": "2",
            "Procesador": "QUAD CORE",
            "Sistema Operativo": "WEBOS",
            "Entradas USB": "1",
            "Potencia Parlantes": "10 W",
            "Alto": " 61,5 CM",
            "Profundidad": "18,7",
            "Ancho": "97,7",
            "Peso": "8KG",
            "Garantía": "12 Meses con el Proveedor"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402945/20402945_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402945/20402945_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402945/20402945_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402945/20402945_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402945/20402945_4.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402945/20402945_5.jpg"
         ]
      },
      "20402949": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20402949,
         "Name": "Smart Tv 55\" Uhd 55Uk6350 ",
         "Brand": "LG",
         "activePrice": "449.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Descripción": "Smart TV 55' UHD 55UK6350 LG",
            "Marca": "LG",
            "Tamaño de Pantalla": "55\"",
            "Resolución": "4K Ultra HD",
            "Tecnología": "LED",
            "Conexión Bluetooth": "SI",
            "Entradas USB": "2",
            "Smart TV": "SI",
            "Entradas HDMI": "3",
            "Control Remoto Incluido": "SI",
            "Tipo": "Televisores",
            "Entradas VGA": "Sin Entradas",
            "Entradas RCA": "1",
            "Entradas Auxiliares 3.5 MM": "No Incluye",
            "Entrada Internet": "SI",
            "Conexión WiFi": "SI",
            "Potencia de los Parlantes": "20 W",
            "Peso": "15 KG",
            "Ancho": "124, 7 CM",
            "Alto": "78,5 CM",
            "Alto sin Base": "78,5 CM",
            "Lentes 3D Incluidos": "NO",
            "Garantía": "1 año",
            "Tipo de Pantalla": "Plana",
            "Hecho en": "No aplica"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402949/20402949_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402949/20402949_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402949/20402949_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402949/20402949_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402949/20402949_4.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402949/20402949_5.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402949/20402949_6.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20402949/20402949_7.jpg"
         ]
      },
      "20403216": {
         "familia": "AUDIO",
         "sku": 20403216,
         "Name": "Parlante Karaoke Bt Black Noise ",
         "Brand": "Master-G",
         "activePrice": "99.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "EAN": "7808748510156",
            "Modelo": "Black noise",
            "Medidas": " 170 x 835 x 335 mm ",
            "Peso": "8,3 kg",
            "Pantalla display LED": "si",
            "iluminacion": "LED RGB",
            "Fashing ": "Desing",
            "Woofer": "5,25\" x2 / tweeter: 2,5\" x2",
            "Potencia Máxima": "90W RMS",
            "Rejilla": "de acero",
            "Conectivdad Bluetooth": "Si",
            "Funcion karaoke": "Si",
            "Reproductor Mp3": "si",
            "Radio FM": "si",
            "Control de volumen y eco para mocrofono": "si",
            "control de tonos": "Control Remoto",
            "Puerto USB": "si",
            "Entradas Auxiliares": "2",
            "Entradas de microfono": "2",
            "Incluye": "Control Remoto"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20403216/20403216_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20403216/20403216_1.jpg"
         ]
      },
      "20404619": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20404619,
         "Name": "Smart Tv 55 Uhd Un55Nu7100Gxzs ",
         "Brand": "Samsung",
         "activePrice": "299.990",
         "CMRprice": null,
         "strikeOutPrice": "429.990",
         "tableData": {
            "Modelo": "UN55NU7100GXZS",
            "Garantía": "1 año",
            "Tipo": "Smart TV",
            "Conexión": "WiFi, USB",
            "Entradas": "HDMI",
            "Tecnología": "LED",
            "Resolución": "4K Ultra HD",
            "Tamaño de Pantalla": "55\"",
            "Origen": "Mexico",
            "Alto": "79,3 cm",
            "Ancho": "123,9 cm",
            "Profundidad": "26,1/5,9 cm",
            "Peso": "17,7 Kg",
            "Color": "Negro",
            "Procesador": "Quad Core"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20404619/20404619_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20404619/20404619_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20404619/20404619_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20404619/20404619_3.jpg"
         ]
      },
      "20404889": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20404889,
         "Name": "Smart Tv 50\" Uhd Un50Nu7100Gxzs ",
         "Brand": "Samsung",
         "activePrice": "259.990",
         "CMRprice": null,
         "strikeOutPrice": "349.990",
         "tableData": {
            "Modelo": "UN50NU7100GXZS",
            "Garantía": "1 año",
            "Tipo": "Smart TV",
            "Conexión": "WiFi, USB",
            "Entradas": "HDMI",
            "Tecnología": "LED",
            "Resolución": "4K Ultra HD",
            "Tamaño de Pantalla": "50\" ",
            "Origen": "Mexico",
            "Alto": "72,88 cm",
            "Ancho": "112,48 cm",
            "Profundidad": "26,13/5,97 cm",
            "Peso": "14,4 Kg",
            "Color": "Negro",
            "Proceador": "Quad Core"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20404889/20404889_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20404889/20404889_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20404889/20404889_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20404889/20404889_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20404889/20404889_4.jpg"
         ]
      },
      "20405932": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20405932,
         "Name": "Smart Tv 49\" Uhd 4K 49Uk6200 ",
         "Brand": "LG",
         "activePrice": "349.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Descripción": "Smart TV 49\" UHD 4K 49UK6200 LG ",
            "Marca": "LG",
            "Tipo de Pantalla": "LCD LED",
            "Pantalla": "49\"",
            "Resolución": "3840*2160",
            "Bluetooth Audio Playback": "SI",
            "Potencia": "20W",
            "LG Sound Sync / Bluetooth": "SI",
            "Procesador": "Quad Core",
            "Conectividad": "HDMI, USB, LAN, WiFi, Bluetooth",
            "Color": "Negro",
            "Base": "2 Patas",
            "Peso": "14,2 KG",
            "Control Remoto": "SI",
            "Manual ": "SI"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20405932/20405932_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20405932/20405932_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20405932/20405932_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20405932/20405932_3.jpg"
         ]
      },
      "20406720": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20406720,
         "Name": "Smart Tv Tcl 49\" Fhd Curvo ",
         "Brand": "TCL",
         "activePrice": "399.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Marca": "TCL",
            "Color": "Gris",
            "Pantalla": "49\"",
            "Resolución": "Full HD",
            "Pixeles": "1920 x 1080",
            "Potencia": "110 W",
            "Sintonizador Digital": "Si",
            "Tecnología": "Smart TV",
            "Puertos": "HDMI, USB 2.0/3.0",
            "Conectividad": "Wi-Fi / Ethernet",
            "Ancho": "109.5 cm",
            "Alto": "69 cm",
            "Profundidad": "24 cm",
            "Peso": "14.5 cm"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20406720/20406720_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20406720/20406720_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20406720/20406720_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20406720/20406720_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20406720/20406720_4.jpg"
         ]
      },
      "20410075": {
         "familia": "CELULARES",
         "sku": 20410075,
         "Name": "Smartphone Galaxy J2 Core Negro Movistar ",
         "Brand": "Movistar",
         "activePrice": "54.990",
         "CMRprice": null,
         "strikeOutPrice": "79.990",
         "tableData": {
            "Modelo": "-",
            "Garantía": "1 año",
            "Color": "Negro",
            "Pocrsador": "Quad-Core 1.4 GHz",
            "Tipo de Producyto": "Celulares Smartphones",
            "Memoria Expandible": "Hasta 64 GB",
            "Cámara Trasera": "8 MP",
            "Cámara Frontal": "5 MP",
            "Memoria RAM": "1.5 GB",
            "Compañía": "Movistar",
            "Sistema Operativo": "Android 7.0 Nougat",
            "Bandas Internet": "GSM GSM850 GSM900 DCS1800 PCS1900 / 3G B1(2100) B2(1900) B4(AWS) B5(850) B8(900) / 4G-LTE: B4(AWS) B7 (2600) B28(700)"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20410075/20410075_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20410075/20410075_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20410075/20410075_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20410075/20410075_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20410075/20410075_4.jpg"
         ]
      },
      "20411830": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20411830,
         "Name": "Smart Tv 55\" L55D1202Smt Rled ",
         "Brand": "Recco",
         "activePrice": "219.990",
         "CMRprice": null,
         "strikeOutPrice": "249.990",
         "tableData": {
            "Descripción": "Smart TV 55\" RLED-L55D1202SMT Recco",
            "Marca": "Recco",
            "Tamaño Pantalla": "55 Pulgadas",
            "Resolución ": "FULL HD",
            "Tecnología ": "LCD",
            "Bluetooth Audio Palyback": "NO",
            "Entradas USB": "2",
            "Smart TV": "SI",
            "Entradas HDMI": "3",
            "Control Remoto": "SI",
            "Modelo ": "RLED-L55D1202SM",
            "Potencia de los Parlantes": "2×8 W",
            "Peso": "13,8 KG",
            "Profundidad": "24.8 CM",
            "Ancho": "124.9 CMS"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20411830/20411830_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20411830/20411830_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20411830/20411830_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20411830/20411830_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20411830/20411830_4.jpg"
         ]
      },
      "20412031": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20412031,
         "Name": "Smart Tv 50\" Uhd 4K Mgu5020X ",
         "Brand": "Master G",
         "activePrice": "299.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20412031/20412031_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20412031/20412031_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20412031/20412031_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20412031/20412031_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20412031/20412031_4.jpg"
         ]
      },
      "20415910": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20415910,
         "Name": "Smart Tv Hd 32\" Un32J4290Agxzs ",
         "Brand": "Samsung",
         "activePrice": "149.990",
         "CMRprice": "139.990",
         "strikeOutPrice": "179.990",
         "tableData": {
            "Descripción": "Smart TV 32 ¿",
            "Marca": "Samsung",
            "Modelo": "UN32J4290AGXZS",
            "Tamaño Pantalla": "32\"",
            "Resolución": "HD",
            "Tecnología": "LCD/Smart TV",
            "Entradas HDMI": "2",
            "Entradas USB ": "1",
            "Control Remoto  Incluido": "SI",
            "Ancho ": "73,74 CMS",
            "Alto con/sin Base": "46,54/43,8 CMS",
            "Profundidad con/sin Base": "15,05/7,41 CMS",
            "Peso": "4,1 KG",
            "Garantía": "12 Meses con el Proveedor"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20415910/20415910_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20415910/20415910_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20415910/20415910_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20415910/20415910_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20415910/20415910_4.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20415910/20415910_5.jpg"
         ]
      },
      "20416071": {
         "familia": "ELECTRO HOGAR",
         "sku": 20416071,
         "Name": "Refrigerador Samsung Side By Side 535Lt  ",
         "Brand": "Samsung",
         "activePrice": " 379.990",
         "CMRprice": "359.990",
         "strikeOutPrice": "699.990",
         "tableData": {
            "Fabrica": "SSEC",
            "Cantidad de Carga": "36",
            "Nr Certificado Seguridad": "E-013-04-9468",
            "NR Certificado Eficiencia": "E-013-04-9479",
            "Código QR": "252532",
            "Eficiencia y Consumo": "A+ [36,40]",
            "Origen": "China",
            "Color": "Easy Clean Steel",
            "Capacidad Total Neta": "535 lt",
            "Capacidad Neta Congelador": "179 lt",
            "Capacidad Neta Refrigeración": "356 lt",
            "Medidas Netas con Manillas/Visagras [cm]": "91,2 cm Ancho x 178,9 cm Alto x 73,4 cm prof",
            "Medidas Brutas [cm]": "97,4 cm Ancho x 190,7 cm Alto x 78,9 cm prof",
            "Medidas Netas sin Manillas/Visagras [cm]": "91,2 cm Ancho x 178,9 cm Alto x 67,2 cm prof",
            "Peso Neto": "98 kg",
            "Peso Bruto": "105 kg",
            "Tensión Nominal / Frecuencia": "220V~240V / 50Hz",
            "Corriente Nominal": "2A",
            "Garantía Producto": "1 año",
            "Garantía del Compresor": "10 años",
            "Grados de Enfriamiento": "4 Estrellas",
            "Refrigerante": "R600a",
            "Incluye Instalación": "No requiere instalacion",
            "Dispensador de Agua Automático": "NO",
            "Dispensador de Hielo ": "NO",
            "Tipo de Ice Maker": "Cubetera",
            "Tipo de Compresor ": "Digital Inverter",
            "Función Power Cool": "SI",
            "Función Power Freeze": "SI",
            "Filtro de Agua": "NO",
            "Control de Temperatura": "Panel LED Digital",
            "Tipo de Manillas": "Manillas",
            "Nr.Bandejas Congelador": "4",
            "Nr. Gavetas Puerta Congelador": "5",
            "Nr. Cajones Congelador": "2",
            "Nr. Bandejas Refrigeración": "5",
            "Nr. Gavetas Puerta Refrigeración": "4",
            "Gavetas de Puerta Ancha": "SI",
            "Nr. Cajones Refrigerador": "2",
            "BandejaHuevos": "SI",
            "Rack para Vinos": "NO",
            "Iluminación ": "LED",
            "Material Bandejas": "Vidrio Templado",
            "Patas Ajustables": "SI",
            "Ruedas de Desplazamiento": "SI",
            "Bloqueo de Panel para niños": "SI",
            "Alarma de Puerta": "SI"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416071/20416071_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416071/20416071_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416071/20416071_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416071/20416071_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416071/20416071_4.jpg"
         ]
      },
      "20416073": {
         "familia": "ELECTRO HOGAR",
         "sku": 20416073,
         "Name": "Lavadora 17 Kg Wa17F7L6Ddw/Zs Blanco ",
         "Brand": "Samsung",
         "activePrice": "259.990",
         "CMRprice": null,
         "strikeOutPrice": "379.990",
         "tableData": {
            "Modelo": "WA17F7L6DDW/ZS",
            "Capacidad de carga": "16 Kg",
            "Tipo de carga": "Superior",
            "Material de tambor": "Acero Inoxidable",
            "Tecnología": "Inverter",
            "Número de programas": "12",
            "Tipo de Pantalla": "Digital",
            "Potencia": "600W",
            "Eficiencia Energética": "No aplica",
            "Tipo de lavadora": "Automática",
            "Ajuste de Temperatura": "Si",
            "Centrifugado": "Si",
            "Peso": "48 Kg",
            "Alto": "103 cm",
            "Ancho": "63 cm",
            "Profundidad": "67 cm",
            "Número de Certificación": "E-013-03-12565",
            "Hecho en": "México"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416073/20416073_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416073/20416073_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416073/20416073_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416073/20416073_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416073/20416073_4.jpg"
         ]
      },
      "20416505": {
         "familia": "ELECTRO HOGAR",
         "sku": 20416505,
         "Name": "Lavadora 11,5 Kg  Efficacce Inox ",
         "Brand": "Mademsa",
         "activePrice": "119.990",
         "CMRprice": null,
         "strikeOutPrice": "189.990",
         "tableData": {
            "Descripción": "Lavadora 11,5KG Efficacce Inox Mademsa",
            "Marca": "Mademsa",
            "Capacidad": "11,5 KG",
            "Consumo de Agua Fría": "110 LTS/Ciclo",
            "Programas de Lavado": "6",
            "Niveles de Agua": "4",
            "Color": "Blanco",
            "Alto ": "96,5 CMS",
            "Ancho": "53,5 CMS",
            "Profundidad": "57,0 CMS",
            "Peso": "30,5 KG",
            "TAMBOR": "Acero Inoxidable"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416505/20416505_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416505/20416505_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416505/20416505_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416505/20416505_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416505/20416505_4.jpg"
         ]
      },
      "20416506": {
         "familia": "ELECTRO HOGAR",
         "sku": 20416506,
         "Name": "Lavadora Mademsa 15,5Kg Efficacce Szg Inox ",
         "Brand": "Mademsa",
         "activePrice": "169.990",
         "CMRprice": "159.990",
         "strikeOutPrice": "229.990",
         "tableData": {
            "Descripción": "Lavadora 15,5KG Efficacce SZG Inox Mademsa ",
            "Marca": "Mademsa",
            "Capacidad": "15,5 KG",
            "Programas de Lavado": "6",
            "Niveles de Agua": "6",
            "Color": "Silver",
            "Alto": "1.040 MM",
            "Ancho": "610 MM",
            "Profundidad": "665 MM",
            "Peso Neto ": "39 KG",
            "Consumo de Agua Fría": "167 Litros/Ciclo"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416506/20416506_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416506/20416506_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416506/20416506_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416506/20416506_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416506/20416506_4.jpg"
         ]
      },
      "20416887": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20416887,
         "Name": "Smart Tv 40\" Line Android Fhd ",
         "Brand": "TCL",
         "activePrice": "169.990",
         "CMRprice": null,
         "strikeOutPrice": "259.990",
         "tableData": {
            "Producto": "Smart TV Line Android FHD 40S6500 TCL",
            "Marca": "TCL",
            "Modelo": "40S6500",
            "Garantía": "1 año",
            "Tecnología": "LED FULL HD",
            "Pantalla": "40\"",
            "Sistema Operativo": "Android TV",
            "Tecnología ": "HDR",
            "Conexión": "WiFi, Bluetooth, USB, 2 HDMI"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416887/20416887_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416887/20416887_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416887/20416887_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416887/20416887_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416887/20416887_4.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20416887/20416887_5.jpg"
         ]
      },
      "20419890": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20419890,
         "Name": "Smart Tv Tcl 65\" Uhd 4K ",
         "Brand": "TCL",
         "activePrice": "399.990",
         "CMRprice": "389.990",
         "strikeOutPrice": "599.990",
         "tableData": {
            "Descripción": "Smart TV 65\" UHD TCL",
            "Marca": "TCL",
            "Modelo ": "P65US",
            "Pantalla": "65\"",
            "Resolución": "3840*2160",
            "Internet": "Cable / Inalambrico",
            "Salida de Audio": "2X8W",
            "Color": "Negro",
            "Control Remoto": "SI ",
            "Manual del Propietario": "SI",
            "USB": "SI",
            "HDMI": "SI",
            "Contraste": "5001   1 "

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20419890/20419890_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20419890/20419890_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20419890/20419890_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20419890/20419890_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20419890/20419890_4.jpg"
         ]
      },
      "20420011": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20420011,
         "Name": "Smart TV 43\" 4K UHD MGU4330X ",
         "Brand": "Master G",
         "activePrice": "159.990",
         "CMRprice": null,
         "strikeOutPrice": "199.990",
         "tableData": {
            "Descripción": "Smart TV 43\" 4K UHD MGU5020X Master-G",
            "Marca": "Master G",
            "Pantalla": "43 Pulgadas",
            "Resolución": "HD",
            "Función": "Smart TV",
            "WiFi": "Integrado",
            "USB": "2",
            "HDMI": "3",
            "Potencia de los Parlantes": "16 Watts",
            "Control Remoto": "SI  ",
            "Peso": "6,9 KG",
            "Ancho ": "97,1 CMS",
            "Alto": "60,7 CMS",
            "Profundidad": "21,3 CMS"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420011/20420011_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420011/20420011_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420011/20420011_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420011/20420011_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420011/20420011_4.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420011/20420011_5.jpg"
         ]
      },
      "20420012": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20420012,
         "Name": "Smart Tv Led 65\" Uhd 4K 65Uk6350Psc.Awh ",
         "Brand": "LG",
         "activePrice": "449.990",
         "CMRprice": null,
         "strikeOutPrice": "899.990",
         "tableData": {
            "Descripción": "Smart TV LED 65\" 65UK6350PSC.AWH UD LG",
            "Marca": "LG",
            "Tamaño Pantalla": "65 PULGADAS",
            "Resolución ": "4K ULTRA HD",
            "Tipo": "LED",
            "Entradas USB": "2",
            "Entradas HDMI": "3",
            "Conexión A Bluetooth": "SI",
            "Control Remoto Incluído": "SI",
            "Procesador ": "Quad Core",
            "Sintonizador Digital ": "SI ",
            "Potencia Parlantes": "20 Watts",
            "Peso": "22,1 KG",
            "Ancho ": "146,8 CMS",
            "Alto con/sin Base": "91,1 CMS",
            "Garantía ": "12 Meses con el Proveedor"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420012/20420012_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420012/20420012_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420012/20420012_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420012/20420012_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420012/20420012_4.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420012/20420012_5.jpg"
         ]
      },
      "20420014": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20420014,
         "Name": "Smart Tv Led 75\" Uhd 4K 75Uk6570Psa.Awh Ud ",
         "Brand": "LG",
         "activePrice": "899.990",
         "CMRprice": null,
         "strikeOutPrice": "1.999.990",
         "tableData": {
            "Tamaño Pantalla": "75 Pulgadas",
            "Smart TV": "SI",
            "Potencia Parlantes": "20 Watts",
            "Entradas USB": "2",
            "Entradas HDMI": "3",
            "Definición": "UHD",
            "Peso": "34,6 KG",
            "Garantía": "12 Meses con el Proveedor"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420014/20420014_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420014/20420014_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420014/20420014_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420014/20420014_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420014/20420014_4.jpg"
         ]
      },
      "20420015": {
         "familia": "AUDIO",
         "sku": 20420015,
         "Name": "Speaker Vintage Wood M10 ",
         "Brand": "Urbano",
         "activePrice": "59.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Marca": "Urbano Vintage",
            "Tipo": "Audio Outdoor",
            "Potencia": "35W",
            "Características": "Bluetooth",
            "Resistente": "Resistente a la Humedad"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420015/20420015_0.jpg"
         ]
      },
      "20420016": {
         "familia": "AUDIO",
         "sku": 20420016,
         "Name": "Speaker Vintage M20 Gray ",
         "Brand": "Urbano",
         "activePrice": "69.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Marca": "Urbano Vintage",
            "Tipo": "Audio Outdoor",
            "Modelo": "UD-SPM202",
            "Potencia RMS": "5W x 2",
            "Potencia": "10W",
            "Ancho": "13 CM",
            "Alto ": "21 CM",
            "Profundidad": "10 CM",
            "Entradas USB": "1",
            "Características": "Bluetooth/Lector SD, Micor SD",
            "Accesorios": "Cable de Carga Y Manual del Usuario",
            "Material": "Acabado de Bambú",
            "Garantía": "3 Meses con el Proveedor",
            "Hecho en": "China"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420016/20420016_0.jpg"
         ]
      },
      "20420017": {
         "familia": "AUDIO",
         "sku": 20420017,
         "Name": "Speaker Vintage M20 Black ",
         "Brand": "Urbano",
         "activePrice": "69.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Marca": "Urbano Vintage",
            "Tipo": "Audio Outdoor",
            "Potencia": "30W",
            "Características": "Bluetooth",
            "Material": "Acabado de Bambú"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420017/20420017_0.jpg"
         ]
      },
      "20420020": {
         "familia": "AUDIO",
         "sku": 20420020,
         "Name": "Speaker Vintage Double Bluetooth T10 ",
         "Brand": "Urbano",
         "activePrice": "69.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Marca": "Urbano Vintage",
            "Tipo": "Audio Outdoor",
            "Potencia": "35W",
            "Caractrísticas": "Compatible con Laptops/ Tablets/ SmartPhones/ Media Players/ Games Consoles",
            "Batería": "Recargable",
            "Resistente": "Resistente a la Humendad"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420020/20420020_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420020/20420020_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420020/20420020_2.jpg"
         ]
      },
      "20420661": {
         "familia": "AUDIO",
         "sku": 20420661,
         "Name": "Parlante Karaoke 250W ",
         "Brand": "Panasonic",
         "activePrice": "59.990",
         "CMRprice": null,
         "strikeOutPrice": "89.990",
         "tableData": {
            "Descripción": "Parlante Karaoke 250W Panasonic  SC-CMAX4PU-K",
            "Marca": "Panasonic",
            "Entradas para Micrófono": "2",
            "Funciones": "Cancelación de Voz, Control de Tono, Control de Compás, Control de Nivel BGM",
            "Ecualizador de Mricrófono": "SI",
            "Potencia": "250 Watts",
            "APP": "Max Jukebox",
            "Efectos": "6 Efectos de Karaoke",
            "Woofer": "20 CM",
            "USB": "SI",
            "Bluetooth": "Corriente Alterna"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420661/20420661_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420661/20420661_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420661/20420661_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420661/20420661_3.jpg"
         ]
      },
      "20420972": {
         "familia": "ELECTRO HOGAR",
         "sku": 20420972,
         "Name": "Refrigerador 235L No Frost Gt26Bgp Silver ",
         "Brand": "LG",
         "activePrice": "249.990",
         "CMRprice": null,
         "strikeOutPrice": "319.990",
         "tableData": {
            "Modelo ": "GT26BPG",
            "Tipo ": "TOP MOUNT",
            "Color": "Grafico",
            "Capacidad": "235 LT",
            "Iluminación": "LED",
            "Material de Bandejas": "Vidrio Templado ",
            "Alto ": "157 CMS",
            "Ancho": "55,5 CMS",
            "Profundidad": "62 CMS",
            "Peso": "49 CMS",
            "Sistema de Frío": "No Frost",
            "Dispensador de Hielo": "NO  ",
            "Dispensador de Agua": "NO ",
            "Garantía ": "1 año ",
            "Garantía del Motor": "10 años"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420972/20420972_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420972/20420972_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420972/20420972_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420972/20420972_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20420972/20420972_4.jpg"
         ]
      },
      "20421399": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20421399,
         "Name": "Smart Tv Samsung 65\" Uhd ",
         "Brand": "Samsung",
         "activePrice": "499.990",
         "CMRprice": null,
         "strikeOutPrice": "679.990",
         "tableData": {
            "Modelo": "Smart TV 65\" UHD 4K NU7100 Samsung",
            "Marca": "Samsung",
            "Resolución": "4K UHD 65\" 3840*2160",
            "Tipo": "Smart TV",
            "Conexiones": "3 HDMI /2 USB",
            "Voltaje": "20W"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20421399/20421399_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20421399/20421399_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20421399/20421399_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20421399/20421399_3.jpg"
         ]
      },
      "20422244": {
         "familia": "ELECTRO HOGAR",
         "sku": 20422244,
         "Name": "Lavadora 13Kg Wt13Wpb Blanca ",
         "Brand": "LG",
         "activePrice": "219.990",
         "CMRprice": null,
         "strikeOutPrice": "299.990",
         "tableData": {
            "Modelo ": "WT13WPB",
            "Tipo de Motor": "Smart Inverter",
            "Color": "Blanco",
            "Capacidad": "13 KG",
            "Tecnología Pulsador": "Turbo Drum",
            "Garantía": "10 años",
            "Lavado Diferido": "SI ",
            "Tipo de Panel": "Digital ",
            "Alto ": "96 CMS",
            "Ancho ": "59 CMS",
            "Profundidad": "60,6 CMS",
            "Peso ": "40KG",
            "Potencia ": "200 Watts",
            "Material de Tapa ": "Vidrio "

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20422244/20422244_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20422244/20422244_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20422244/20422244_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20422244/20422244_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20422244/20422244_4.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20422244/20422244_5.jpg"
         ]
      },
      "20423020": {
         "familia": "ELECTRO HOGAR",
         "sku": 20423020,
         "Name": "Secadora Mademsa 7kg Solare 7100 ",
         "Brand": "Mademsa",
         "activePrice": "159.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Descripción": "Secadora 7KG Solare 7100 Mademsa",
            "Marca": "Mademsa",
            "Capacidad": "7KG ",
            "Panel": "Digital de uso Intuitivo",
            "Alto": "84 CMS",
            "Ancho": "59,5 CMS",
            "Fondo": "55 CMS",
            "Consumo de Energía": " 2,25 KWH",
            "Niveles de Tempreratura": "3",
            "Programas de Secado": "4",
            "Filtro Pelusas Removible": "SI"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20423020/20423020_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20423020/20423020_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20423020/20423020_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20423020/20423020_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20423020/20423020_4.jpg"
         ]
      },
      "20426138": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20426138,
         "Name": "Smart TV 43\" FHD J5202 ",
         "Brand": "Samsung",
         "activePrice": "199.990",
         "CMRprice": null,
         "strikeOutPrice": "249.990",
         "tableData": {
            "Descripción": "Smart TV  43¿ FHD   ",
            "Marca": "Samsung",
            "Modelo": "UN43J5202AGXZS",
            "Tamaño Pantalla": "43\"",
            "Resolución": "FULL HD",
            "Tecnología": "Smart TV",
            "Entradas HDMI": "2",
            "Entradas USB ": "1",
            "Control Remoto  Incluido": "SI",
            "Ancho ": "96,7 CMS",
            "Alto con/sin Base": "61,9/57,1 CMS ",
            "Profundidad con/sin Base": "22,6/8 CMS",
            "Peso": "7,4 CMS",
            "Garantía": "12 Meses con el Proveedor"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20426138/20426138_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20426138/20426138_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20426138/20426138_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20426138/20426138_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20426138/20426138_4.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20426138/20426138_5.jpg"
         ]
      },
      "20426595": {
         "familia": "TELEVISORES Y VIDEOJUEGOS",
         "sku": 20426595,
         "Name": "Smart TV 50\" UHD 4K 7095 ",
         "Brand": "Samsung",
         "activePrice": "259.990",
         "CMRprice": null,
         "strikeOutPrice": "349.990",
         "tableData": {
            "Marca": "Samsung",
            "Modelo": "UN50NU7095GXZS",
            "Tamaño Pantalla": "50\"",
            "Resolución": "4K ULTRA HD",
            "Tecnología": "Smart TV",
            "Procesador": "QUAD CORE",
            "Control Remoto Incluido": "SI",
            "Potencia Parlantes": "20 Watts",
            "Ancho ": "112,2 CMS",
            "Alto con/sin Base": "69,3 CMS / 64,9 CMS",
            "Profundidad": "24 CMS",
            "Peso": "9,5 KG",
            "WiFi": "SI",
            "Garantía": "12 Meses con el Proveedor"

         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20426595/20426595_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20426595/20426595_1.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20426595/20426595_2.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20426595/20426595_3.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20426595/20426595_4.jpg"
         ]
      },
      "20427483": {
         "familia": "ELECTRO HOGAR",
         "sku": 20427483,
         "Name": "Lavadora 8Kg WA80H4200SW/ZS ",
         "Brand": "Samsung",
         "activePrice": "169.990",
         "CMRprice": null,
         "strikeOutPrice": null,
         "tableData": {
            "Marca": "Samsung",
            "Capacidad": "8 KG",
            "Color": "Blanco",
            "Pantalla": "Dual Cluster/ Inlay",
            "Air Turbo": "SI",
            "Visor de Vidrio Templado ": "SI",
            "Pulsador": "Wobble",
            "Niveles de Agua": "5 Niveles",
            "Tipo de Tambor": "Diamante",
            "Bloqueo de Niños": "SI ",
            "Cantidad de Ciclos": "6",
            "Peso": "31 KG"
         },
         "picArray": [
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20427483/20427483_0.jpg",
            "http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/20427483/20427483_1.jpg"
         ]
      }
   }
   productData = null;
   expired = true;
   tabData = { title: "default", rootString: "default", icon: "default", filter: "default" };
   tabInfo = []

   public lastDownloadTime = "Nunca Actualizado";

   constructor(
      public http: HttpClient,
      public platform: Platform,
      private storage: Storage
   ) {

      console.log('Hello DataProvider Provider beer');

      this.getTabInfo()

      this.platform.resume.subscribe(() => {
         console.log("App has been resumed")
         this.getValues()

      })

   }

   async getTabInfo() {
      console.log("entered getTabInfo in data provider")
      // this.tabInfo = [
      //    { title: "Todo", rootString: "todoPage", icon: "refresh-circle", filter: "None" },
      //    // { title: "Audio", rootString: "templatePage", icon: "custom-audio", filter: "AUDIO" },
      //    // { title: "Celulares", rootString: "templatePage", icon: "custom-phone", filter: "CELULARES" },
      //    // { title: "Electro Hogar", rootString: "templatePage", icon: "custom-electroHogar", filter: "ELECTRO HOGAR" },
      //    // { title: "Televisores y Video Juegos", rootString: "templatePage", icon: "custom-tv", filter: "TELEVISORES Y VIDEOJUEGOS" },
      //    // { title: "Más", rootString: "templatePage", icon: "custom-boxes", filter: "MAS" },
      //    // { title: "Ofertas", rootString: "templatePage", icon: "custom-pricetags", filter: "None" },
      //    { title: "Actualizar", rootString: "downloadPage", icon: "download" },
      // ];

      let val = await this.getSavedData("tabInfo")
      console.log("val data inside getSavedData")
      console.log(val)
      console.log("inside getSasedData")
      this.tabInfo = val;
      console.log(this.tabInfo)


      console.log("leaving getTabInfo()")
   }



   getValues() {
      console.log("entered getValues()")
      this.getSavedData("lastUpdate")
         .then((val) => {
            console.log("inside then statement for lastDownloadTime")
            this.lastDownloadTime = val

            this.getSavedData("productData")
               .then(val => {
                  this.checkValues(val)

                  this.getSavedData("tabInfo")
                     .then(val => {
                        this.tabInfo = val
                     })

               })
         })
   }

   checkValues(val) {
      if (moment().format('MMMM Do YYYY').toString() != moment(this.lastDownloadTime, 'MMMM Do YYYY, h:mm a').format('MMMM Do YYYY').toString()) {
         this.productData = {}
      } else {
         this.productData = val
      }
   }

   async getSavedData(tag) {
      let data = null
      // Or to get a key/value pair
      await this.storage.get(tag).then(async (val) => {
         data = val
      });
      return data
   }

}


