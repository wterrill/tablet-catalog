import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ElectroHogarPage } from './electro-hogar';

@NgModule({
  declarations: [
    ElectroHogarPage,
  ],
  imports: [
    IonicPageModule.forChild(ElectroHogarPage),
  ],
})
export class ElectroHogarPageModule {}
