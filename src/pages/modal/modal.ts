import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { PhotoViewer } from '@ionic-native/photo-viewer'
import { File } from '@ionic-native/file';



/**
 * Generated class for the ModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: 'modal.html',
})
export class ModalPage {
  private productData = null;
  private objectKeys = Object.keys;
  private showTable = true;

  constructor(
    private navCtrl: NavController, 
    private navParams: NavParams, 
    private viewCtrl: ViewController,
    private photo: PhotoViewer,
    private file: File
    ) {
      this.productData = this.navParams.get('data')
      console.log(this.productData)
      if(!this.productData.hasOwnProperty('tableData')){
        this.showTable = false;
    }

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalPage');
  }

  dismiss() {
    let data = { 'foo': 'bar' };
    this.viewCtrl.dismiss(data);
  }

  openPhoto(url){
    let url2 = this.file.dataDirectory + url.replace("http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/","")
    var options = {
      share: true, // default is false
      closeButton: false, // default is true
      copyToReference: true // default is false
      //headers: '"Beer"',  // If this is not provided, an exception will be triggered
      //piccasoOptions: { } // If this is not provided, an exception will be triggered
  };
    this.photo.show(url2,"",options);
  }

}
