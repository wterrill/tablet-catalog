import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DataTroubleshootingPage } from './data-troubleshooting';

@NgModule({
  declarations: [
    DataTroubleshootingPage,
  ],
  imports: [
    IonicPageModule.forChild(DataTroubleshootingPage),
  ],
})
export class DataTroubleshootingPageModule {}
