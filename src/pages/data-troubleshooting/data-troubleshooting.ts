import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';

/**
 * Generated class for the DataTroubleshootingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-data-troubleshooting',
  templateUrl: 'data-troubleshooting.html',
})
export class DataTroubleshootingPage {
  private appDataShow = null;
  private keys = null;
  private objectKeys = Object.keys;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    //private file: File,
    private appData: DataProvider
  ) {
    this.appDataShow = this.appData.productData
    this.keys = Object.keys(this.appDataShow)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DataTroubleshootingPage');
  }

  
}