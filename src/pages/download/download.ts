import * as moment from 'moment';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import { HttpClient } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { DataProvider } from '../../providers/data/data'
import { Storage } from '@ionic/storage';
import { AlertController } from 'ionic-angular'
//import { TabsPage } from '../tabs/tabs'



@IonicPage()
@Component({
  selector: 'page-download',
  templateUrl: 'download.html',
})
export class DownloadPage {

  private fileTransfer: FileTransferObject;
  private win: any = window;
  private toggle = false;
  private loadProgress = 0;
  //private lastDownloadTime = null;
  private skus =
    [
      ["ELECTRO HOGAR", 20198061],
      ["ELECTRO HOGAR", 20219387],
      ["ELECTRO HOGAR", 20222047],
      ["ELECTRO HOGAR", 20227267],
      ["ELECTRO HOGAR", 20231088],
      ["ELECTRO HOGAR", 20245279],
      ["ELECTRO HOGAR", 20256212],
      ["ELECTRO HOGAR", 20263061],
      ["ELECTRO HOGAR", 20278337],
      ["ELECTRO HOGAR", 20282544],
      ["ELECTRO HOGAR", 20311860],
      ["ELECTRO HOGAR", 20317613],
      ["ELECTRO HOGAR", 20330069],
      ["ELECTRO HOGAR", 20359286],
      ["ELECTRO HOGAR", 20366838],
      ["ELECTRO HOGAR", 20370892],
      ["ELECTRO HOGAR", 20370893],
      ["ELECTRO HOGAR", 20376225],
      ["ELECTRO HOGAR", 20376226],
      ["ELECTRO HOGAR", 20376227],
      ["ELECTRO HOGAR", 20377536],
      ["ELECTRO HOGAR", 20377537],
      ["ELECTRO HOGAR", 20391224],
      ["ELECTRO HOGAR", 20392649],
      ["ELECTRO HOGAR", 20403740],
      ["ELECTRO HOGAR", 20406715],
      ["ELECTRO HOGAR", 20406764],
      ["ELECTRO HOGAR", 20416071],
      ["ELECTRO HOGAR", 20416073],
      ["ELECTRO HOGAR", 20416503],
      ["ELECTRO HOGAR", 20416504],
      ["ELECTRO HOGAR", 20416505],
      ["ELECTRO HOGAR", 20416506],
      ["ELECTRO HOGAR", 20420972],
      ["ELECTRO HOGAR", 20422244],
      ["ELECTRO HOGAR", 20422891],
      ["ELECTRO HOGAR", 20423020],
      ["ELECTRO HOGAR", 20423021],
      ["ELECTRO HOGAR", 20425030],
      ["ELECTRO HOGAR", 20427483],
      ["TELEVISORES Y VIDEOJUEGOS", 20306742],
      ["TELEVISORES Y VIDEOJUEGOS", 20340560],
      ["TELEVISORES Y VIDEOJUEGOS", 20349044],
      ["TELEVISORES Y VIDEOJUEGOS", 20398518],
      ["TELEVISORES Y VIDEOJUEGOS", 20398519],
      ["TELEVISORES Y VIDEOJUEGOS", 20398520],
      ["TELEVISORES Y VIDEOJUEGOS", 20401883],
      ["TELEVISORES Y VIDEOJUEGOS", 20402734],
      ["TELEVISORES Y VIDEOJUEGOS", 20402944],
      ["TELEVISORES Y VIDEOJUEGOS", 20402945],
      ["TELEVISORES Y VIDEOJUEGOS", 20402946],
      ["TELEVISORES Y VIDEOJUEGOS", 20402948],
      ["TELEVISORES Y VIDEOJUEGOS", 20402949],
      ["TELEVISORES Y VIDEOJUEGOS", 20404619],
      ["TELEVISORES Y VIDEOJUEGOS", 20404889],
      ["TELEVISORES Y VIDEOJUEGOS", 20405932],
      ["TELEVISORES Y VIDEOJUEGOS", 20406720],
      ["TELEVISORES Y VIDEOJUEGOS", 20406721],
      ["TELEVISORES Y VIDEOJUEGOS", 20411497],
      ["TELEVISORES Y VIDEOJUEGOS", 20411498],
      ["TELEVISORES Y VIDEOJUEGOS", 20411830],
      ["TELEVISORES Y VIDEOJUEGOS", 20412031],
      ["TELEVISORES Y VIDEOJUEGOS", 20415910],
      ["TELEVISORES Y VIDEOJUEGOS", 20416887],
      ["TELEVISORES Y VIDEOJUEGOS", 20416889],
      ["TELEVISORES Y VIDEOJUEGOS", 20419890],
      ["TELEVISORES Y VIDEOJUEGOS", 20419891],
      ["TELEVISORES Y VIDEOJUEGOS", 20420011],
      ["TELEVISORES Y VIDEOJUEGOS", 20420012],
      ["TELEVISORES Y VIDEOJUEGOS", 20420013],
      ["TELEVISORES Y VIDEOJUEGOS", 20420014],
      ["TELEVISORES Y VIDEOJUEGOS", 20421399],
      ["TELEVISORES Y VIDEOJUEGOS", 20424789],
      ["TELEVISORES Y VIDEOJUEGOS", 20424790],
      ["TELEVISORES Y VIDEOJUEGOS", 20424791],
      ["TELEVISORES Y VIDEOJUEGOS", 20424792],
      ["TELEVISORES Y VIDEOJUEGOS", 20424795],
      ["TELEVISORES Y VIDEOJUEGOS", 20426138],
      ["TELEVISORES Y VIDEOJUEGOS", 20426595],
      ["AUDIO", 20297791],
      ["AUDIO", 20339435],
      ["AUDIO", 20339436],
      ["AUDIO", 20362174],
      ["AUDIO", 20369688],
      ["AUDIO", 20369690],
      ["AUDIO", 20370241],
      ["AUDIO", 20377922],
      ["AUDIO", 20402896],
      ["AUDIO", 20403216],
      ["AUDIO", 20408982],
      ["AUDIO", 20410193],
      ["AUDIO", 20411240],
      ["AUDIO", 20419482],
      ["AUDIO", 20419483],
      ["AUDIO", 20419499],
      ["AUDIO", 20420015],
      ["AUDIO", 20420016],
      ["AUDIO", 20420017],
      ["AUDIO", 20420018],
      ["AUDIO", 20420019],
      ["AUDIO", 20420020],
      ["AUDIO", 20420021],
      ["AUDIO", 20420022],
      ["AUDIO", 20420023],
      ["AUDIO", 20420661],
      ["AUDIO", 20420969],
      ["AUDIO", 20421195],
      ["AUDIO", 20422298],
      ["AUDIO", 20422300],
      ["CELULARES", 20431777],
      ["CELULARES", 20431776],
      ["CELULARES", 20431779],
      ["CELULARES", 20386529],
      ["CELULARES", 20431150],
      ["AUDIO", 20427648],
      ["AUDIO", 20427649],
      ["CELULARES", 20430693],
      ["AUDIO", 20332601],
      ["AUDIO", 20332602],
      ["AUDIO", 20332600],
      ["ELECTRO HOGAR", 20297533],
      ["ELECTRO HOGAR", 20340551],
      ["ELECTRO HOGAR", 20272439],
      ["ELECTRO HOGAR", 20334601],
      ["ELECTRO HOGAR", 20198063],
      ["ELECTRO HOGAR", 20363144],
      ["ELECTRO HOGAR", 20356815],
      ["CELULARES", 20410075]
    ]
  private showFile = null;
  private missingPics = []
  private missingScene7 = []
  private currentCount = 0;
  private totalCount = 0;
  private showCount = false;
  private connectionStatus = false;

  tabInfo = [
    { title: "Todo", rootString: "todoPage", icon: "refresh-circle", filter: "None" },
    // { title: "Audio", rootString: "templatePage", icon: "custom-audio", filter: "AUDIO" },
    { title: "Celulares", rootString: "templatePage", icon: "custom-phone", filter: "CELULARES" },
    // { title: "Electro Hogar", rootString: "templatePage", icon: "custom-electroHogar", filter: "ELECTRO HOGAR" },
    // { title: "Televisores y Video Juegos", rootString: "templatePage", icon: "custom-tv", filter: "TELEVISORES Y VIDEOJUEGOS" },
    { title: "Más", rootString: "templatePage", icon: "custom-boxes", filter: "MAS" },
    { title: "Ofertas", rootString: "templatePage", icon: "custom-pricetags", filter: "None" },
    { title: "Actualizar", rootString: "downloadPage", icon: "download" },
  ];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private http: HTTP,
    private transfer: FileTransfer,
    private file: File,
    private appData: DataProvider,
    private storage: Storage,
    private alertCtrl: AlertController,
    //private tabspage: TabsPage
  ) {
    console.log("inside constructor")
    this.getValues()

  }

  ionViewDidLoad() {

  }

  async ionViewDidEnter() {
    console.log('ionViewDidEnter DownloadPage');
    //this.totalCount = 0
    //this.currentCount = 0
    //this.showFile = this.win.Ionic.WebView.convertFileSrc(this.file.dataDirectory + "imgs/" + "20426138/20426138_2.jpg");

  }

  getValues() {
    console.log("entered setValues()")

    this.getSavedData("lastUpdate")
      .then((val) => {
        console.log("inside then statement for lastDownloadTime")
        this.appData.lastDownloadTime = val

        this.getSavedData("productData")
          .then(val => {
            this.checkValues(val)
          })
      })
  }

  checkValues(val) {
    console.log(moment().diff(moment(this.appData.lastDownloadTime, 'MMMM Do YYYY, h:mm a')))
    console.log(moment().format('MMMM Do YYYY').toString())
    console.log(moment(this.appData.lastDownloadTime, 'MMMM Do YYYY, h:mm a').format('MMMM Do YYYY').toString())
    var beer = false
    // while(!beer){
    //   console.log("beer")
    // }
    if (this.appData.lastDownloadTime == null) {
      this.appData.expired = true
      this.appData.productData = {}
    } else if (moment().format('MMMM Do YYYY').toString() != moment(this.appData.lastDownloadTime, 'MMMM Do YYYY, h:mm a').format('MMMM Do YYYY').toString()) {
      this.appData.expired = true
      this.appData.productData = {}
    } else {
      this.appData.productData = val
      this.appData.expired = false;
    }
  }


  toggleToggle() {
    this.toggle = !this.toggle;
  }
  //  https://www.tottus.cl/static/img/productos/  sku  _ 1,2,3,etc .jpg      <-- image

  //  https://www.tottus.cl/tottus/search?Ntt=  sku                           <-- Search

  //  https://us-central1-commercetoolstottus.cloudfunctions.net/app/testcms  <-- Endpoint

  async transferFiles_FROM_CMS() {
    await this.testConnection()
    if (this.connectionStatus) {
      this.currentCount = 0
      this.showCount = true;
      let data = await this.getApiData()
      let skus = Object.keys(data)
      this.totalCount = skus.length
      for (var i = 0; i < skus.length; i++) {
        this.currentCount++
        this.loadProgress = Math.round((this.currentCount / this.totalCount) * 100)
        let current_sku = skus[i];
        data[current_sku]["picDownloadArray"] = await this.getPicsFromArray(current_sku, data[current_sku].picArray)
      }
      this.saveData("productData", data)
      let tempTime = moment().format('MMMM Do YYYY, h:mm a').toString()
      this.saveData("lastUpdate", tempTime)

      this.saveData("tabInfo", this.tabInfo)
      //this.tabspage.modifyTabs()


      this.appData.lastDownloadTime = tempTime
      this.appData.productData = data
    } else {
      console.log("There is no connection")
      this.presentAlertWifi()
    }
  }

  async getApiData() {

    let finalData = null
    await this.http.get("https://us-central1-commercetoolstottus.cloudfunctions.net/app/testcms", {}, {})
      .then((data) => {
        console.log(data)
        finalData = JSON.parse(data.data).results
      })
      .catch((error) => {

      })
    finalData = this.convertArrayToObject(finalData, "sku")
    return finalData
  }

  convertArrayToObject(array, key) {
    let tempObject = {}
    for (var i = 0; i < array.length; i++) {
      tempObject[array[i][key]] = array[i]
    }
    return tempObject
  }

  async getSavedData(tag) {
    let data = null
    // Or to get a key/value pair
    await this.storage.get(tag).then(async (val) => {
      data = val
    });
    return data
  }

  saveData(tag, data) {
    // set a key/value
    this.storage.set(tag, data);
  }

  async testConnection() {
    this.http.setRequestTimeout(5);
    await this.http.get("https://commercetoolstottus.firebaseio.com/StoreProducts.json", {}, {})
      .then(async x => {
        this.connectionStatus = true
      })
      .catch(err => {
        this.connectionStatus = false
      })
  }

  presentAlertWifi() {
    let alert = this.alertCtrl.create({
      title: 'No hay internet disponible',
      subTitle: 'Por favor, conecta a un red de wifi, y intenta de nuevo',
      buttons: ['OK']
    });
    alert.present();
  }

  checkFile(path: string) {
    path = "20415910_1.jpg"
    this.file.checkFile(this.file.dataDirectory, path)
      .then((x) => {
        console.log(x)
        console.log("exist!!!");  // I always enter here
      })
      .catch((err) => {
        // try again
        console.log("ERR : " + err);
      });
  }

  async checkDir(path) {
    let result = false;
    await this.file.checkDir(this.file.dataDirectory, path)
      .then(x => {
        console.log("entered in checkDir")
        console.log(x)
        result = true;

      })
      .catch(err => {
        console.log("failed checkDir")
      })
    console.log("path = ", path)
    console.log("result =", result)
    return result
  }

  async delete_Files_FROM_CMS() {
    console.log("entered")
    var folderExists = await this.checkDir("imgs")
    console.log("folderExist = ", folderExists)
    if (folderExists) {
      let beer = await this.file.removeRecursively(this.file.dataDirectory, "imgs/")
    }
    this.transferFiles_FROM_CMS()
  }

  async getPicsFromArray(sku, picArray) {
    let alreadyThere = await this.checkDir("imgs/"+sku)
    let stillMorePics = true
    let picDownloadArray = []
    if (!alreadyThere) {
      for (var i = 0; i < picArray.length; i++) {
        let filename = sku + "_" + i + ".jpg"
        let url2 = picArray[i]
        let picInfo = await this.downloadFile(filename, url2, sku)

        if (picInfo[1]) { //picInfo 1 tells if it was successful or not
          picDownloadArray.push(picInfo[0])
          console.log(picInfo[0])
        }
      }
    } else {
      for (var i = 0; i < picArray.length; i++) {
        picDownloadArray.push("http://localhost/_app_file_/data/user/0/com.terrilltech.electroTottus/files/imgs/" + sku + "/" + sku + "_" + i + ".jpg")
      }
    }
    if (picDownloadArray.length < 2) {
      this.missingPics.push(sku)
    }
    console.log("picDownloadArray")
    console.log(picDownloadArray)
    return picDownloadArray
  }

  async downloadFile(filename, url, sku) {
    let picSuccess = false
    const fileTransfer: FileTransferObject = this.transfer.create();
    let downloadPath = this.file.dataDirectory + "imgs/" + sku + "/";
    let showPath = this.win.Ionic.WebView.convertFileSrc(downloadPath + filename);
    await fileTransfer.download(url, downloadPath + filename).then((entry) => {
      console.log("!!success downloading ", sku, " as ", filename);
      picSuccess = true
    }, (error) => {
      // handle error
      console.log("error downloading ", sku, " as ", filename);
    });
    //console.log(picSuccess)
    return [showPath, picSuccess]
  }
}
