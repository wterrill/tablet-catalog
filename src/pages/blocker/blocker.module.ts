import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BlockerPage } from './blocker';

@NgModule({
  declarations: [
    BlockerPage,
  ],
  imports: [
    IonicPageModule.forChild(BlockerPage),
  ],
})
export class BlockerPageModule {}
