import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Cordova } from '@ionic-native/core';

/**
 * Generated class for the BlockerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-blocker',
  templateUrl: 'blocker.html',
})
export class BlockerPage {
  private minVersion = null;
  private ultVersion = null;
  private appVersion = null;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.minVersion = navParams.get('minVersion')
    this.ultVersion = navParams.get('ultVersion')
    this.appVersion = navParams.get('appVersion')

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BlockerPage');
  }

  launch_URL() {
    window.open("https://play.google.com/store/apps/details?id=com.terrilltech.electroTottus", '_system', 'location=yes');
  }

  //

}
