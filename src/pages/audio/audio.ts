import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';



@IonicPage()
@Component({
  selector: 'page-audio',
  templateUrl: 'audio.html',
})
export class AudioPage {

  private appDataShow = null;
  private keys = null;
  private noData = true;
  private expired = false;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private modalCtrl: ModalController,
    private appData: DataProvider
    ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AudioPage');
  }

  ionViewDidEnter() {
    console.log("Entered AudioPage")
    var result = {}, key;
    var ranking={};

    for ( key in this.appData.productData){
      if( this.appData.productData[key].familia == "AUDIO" ) {
        result[key] = this.appData.productData[key];
      }
    }
    /** CAMBIO RANKING */
    console.log('ionViewDidLoad AudioPage');
 
    this.appDataShow = result


    /* START STEFY: AQUI PUEDO REVISAR RANKING */ 
    var rankingcat= []
    var orderok=[]
    /*Obtener ranking*/ 
    for (var i in this.appDataShow) {
      rankingcat.push(this.appDataShow[i].ranking_cat);
      console.log("sku "+i+" ranking: " + this.appDataShow[i].ranking_cat);
    }
    /* Ordenar de menor a mayor por RANKING */
    for(let x = 0; x < rankingcat.length; x++){
      for(let i = 0; i < rankingcat.length-x-1; i++){
          if(rankingcat[i]>rankingcat[i+1]){
              let tmp=rankingcat[i+1];
              rankingcat[i+1]=rankingcat[i];
              rankingcat[i]=tmp;
          }
      }
    }
    /*Volver a ordenar por sku*/ 
    for(let numero = 0; numero < rankingcat.length; numero++){
      for (var keys in this.appDataShow){
        if( this.appDataShow[keys].ranking_cat == rankingcat[numero] ) {
          orderok.push(this.appData.productData[keys].sku);
          console.log("order ok ranking "+numero+" sku: " + this.appData.productData[keys].sku);
        } 
      }
    }
    /* END STEFY */

    /*this.keys = Object.keys(this.appDataShow)*/
    this.keys=orderok;
    
    if (this.keys.length == 0){
      this.noData = true;
    } else {
      this.noData = false
    }
  }

  async openModal(sku){
    let modal = this.modalCtrl.create(
      'ModalPage', 
      {data : this.appDataShow[sku]},
      {cssClass: "my-modal"}
    );
    modal.onDidDismiss(data => {
      console.log(data);
    });
    return await modal.present();
  }


}
