import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data'


@IonicPage()
@Component({
  selector: 'page-offers',
  templateUrl: 'offers.html',
})
export class OffersPage {
  private appDataShow = null;
  private keys = null;
  private noData = true;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private appData: DataProvider,
    private modalCtrl: ModalController
  ) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OffersPage');

  }

  ionViewDidEnter() {
    console.log("Entered OffersPage")
    // This code filters objects by the familia
    var result = {}, key;
    for (key in this.appData.productData) {
      if ('ranking_oferta'in this.appData.productData[key]) {
        result[key] = this.appData.productData[key];
      }
    }
    this.appDataShow = result
    /* START STEFY: AQUI PUEDO REVISAR RANKING */ 
    var rankingoferta= []
    var orderok=[]
    /*Obtener ranking*/ 
    for (var i in this.appDataShow) {
      rankingoferta.push(this.appDataShow[i].ranking_oferta);
      console.log("sku "+i+" ranking: " + this.appDataShow[i].ranking_oferta);
    }
    /* Ordenar de menor a mayor por RANKING */
    for(let x = 0; x < rankingoferta.length; x++){
      for(let i = 0; i < rankingoferta.length-x-1; i++){
          if(rankingoferta[i]>rankingoferta[i+1]){
              let tmp=rankingoferta[i+1];
              rankingoferta[i+1]=rankingoferta[i];
              rankingoferta[i]=tmp;
          }
      }
    }
    /*Volver a ordenar por sku*/ 
    for(let numero = 0; numero < rankingoferta.length; numero++){
      for (var keys in this.appDataShow){
        if( this.appDataShow[keys].ranking_oferta == rankingoferta[numero] ) {
          orderok.push(this.appData.productData[keys].sku);
          console.log("order ok ranking "+numero+" sku: " + this.appData.productData[keys].sku);
        } 
      }
    }
    /* END STEFY */

    /*this.keys = Object.keys(this.appDataShow)*/
    this.keys=orderok;
    
    if (this.keys.length == 0){
      this.noData = true;
    } else {
      this.noData = false
    }
  }

  async openModal(sku) {
    let modal = this.modalCtrl.create(
      'ModalPage',
      { data: this.appDataShow[sku] },
      { cssClass: "my-modal" }
    );
    modal.onDidDismiss(data => {
      console.log(data);
    });
    return await modal.present();
  }

}