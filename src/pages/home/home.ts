import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private appDataShow = null;
  private keys = null;
  private objectKeys = Object.keys;
  constructor(
    public navCtrl: NavController,
    private appData: DataProvider,
    private modalCtrl: ModalController
    ){
      this.appDataShow = this.appData.productData
      this.keys = Object.keys(this.appDataShow)
  }

  async openModal(sku){
    let modal = this.modalCtrl.create(
      'ModalPage', 
      {data : this.appDataShow[sku]},
      {cssClass: "my-modal"}
    );
    modal.onDidDismiss(data => {
      console.log(data);
    });
    return await modal.present();
  }
}