import { Component } from '@angular/core';
import { NavController, ModalController, IonicPage } from 'ionic-angular';
import { DataProvider } from '../../providers/data/data';
import { PhotoViewer } from '@ionic-native/photo-viewer'

@IonicPage()
@Component({
  selector: 'page-todos',
  templateUrl: 'todos.html',
})
export class TodosPage {
  private appDataShow = null;
  private keys = null;
  private objectKeys = Object.keys;
  private noData = true;
  constructor(
    public navCtrl: NavController,
    private appData: DataProvider,
    private modalCtrl: ModalController,
    private photo: PhotoViewer
    ){

  }

  ionViewDidLoad() {

  }

  ionViewDidEnter() {
    console.log("Entered TodosPage")
    var result = {}, key;
    for (key in this.appData.productData) {
      if (this.appData.productData[key].hasOwnProperty("familia")) {
        result[key] = this.appData.productData[key];
      }
    }
    this.appDataShow = result
        /* START STEFY: AQUI PUEDO REVISAR RANKING */ 
        var rankingtodo= []
        var orderok=[]
        /*Obtener ranking*/ 
        for (var i in this.appDataShow) {
          rankingtodo.push(this.appDataShow[i].ranking_todo);
          console.log("sku "+i+" ranking: " + this.appDataShow[i].ranking_todo);
        }
        /* Ordenar de menor a mayor por RANKING */
        for(let x = 0; x < rankingtodo.length; x++){
          for(let i = 0; i < rankingtodo.length-x-1; i++){
              if(rankingtodo[i]>rankingtodo[i+1]){
                  let tmp=rankingtodo[i+1];
                  rankingtodo[i+1]=rankingtodo[i];
                  rankingtodo[i]=tmp;
              }
          }
        }
        /*Volver a ordenar por sku*/ 
        for(let numero = 0; numero < rankingtodo.length; numero++){
          for (var keys in this.appDataShow){
            if( this.appDataShow[keys].ranking_todo == rankingtodo[numero] ) {
              orderok.push(this.appData.productData[keys].sku);
              console.log("order ok ranking "+numero+" sku: " + this.appData.productData[keys].sku);
            } 
          }
        }
        /* END STEFY */
    
        /*this.keys = Object.keys(this.appDataShow)*/
        this.keys=orderok;
    
    if (this.keys.length == 0){
      this.noData = true;
    } else {
      this.noData = false
    }
  }

  async openModal(sku){
    let modal = this.modalCtrl.create(
      'ModalPage', 
      {data : this.appDataShow[sku]},
      {cssClass: "my-modal"}
    );
    modal.onDidDismiss(data => {
      console.log(data);
    });
    return await modal.present();
  }
}