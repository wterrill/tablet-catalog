import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TvvjPage } from './tvvj';

@NgModule({
  declarations: [
    TvvjPage,
  ],
  imports: [
    IonicPageModule.forChild(TvvjPage),
  ],
})
export class TvvjPageModule {}
