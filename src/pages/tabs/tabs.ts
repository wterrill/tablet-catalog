import { Component, ViewChild } from '@angular/core';
import { Platform, NavController } from 'ionic-angular';
import { Tabs, Tab } from 'ionic-angular';

import { HomePage } from '../home/home';
import { BlockerPage } from '../blocker/blocker'
import { TodosPage } from '../todos/todos';
import { AudioPage } from '../audio/audio';
import { CelularesPage } from '../celulares/celulares';
import { ElectroHogarPage } from '../electro-hogar/electro-hogar';
import { PlpPage } from '../plp/plp';
import { TvvjPage } from '../tvvj/tvvj';
import { MasPage } from '../mas/mas';
import { DownloadPage } from '../download/download';
import { OffersPage } from '../offers/offers';
import { DataTroubleshootingPage } from '../data-troubleshooting/data-troubleshooting';
import { AppVersion } from '@ionic-native/app-version';
import { HTTP } from '@ionic-native/http/';
import { DataProvider } from '../../providers/data/data';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})

export class TabsPage {
  @ViewChild('myTabs') tabRef: Tabs;
  private clickCount = 0;
  private showBug = false
  private dynamicTabs = false;
  selectedIndex = this.appData.tabInfo.length - 1

  tab1Root = TodosPage;
  tab2Root = AudioPage;
  tab3Root = CelularesPage;
  tab4Root = ElectroHogarPage;
  tab5Root = TvvjPage;
  tab6Root = MasPage;
  tab7Root = OffersPage;
  tab8Root = DownloadPage;

  //tab7Root = DataTroubleshootingPage
  tab9Root = BlockerPage;
  templatePage = PlpPage;
  todoPage = TodosPage;
  showTabs = false;
  firstTime = true;
  audioPage = AudioPage
  celularesPage = CelularesPage
  electroHogarPage = ElectroHogarPage
  tvvjPage = TvvjPage
  masPage = MasPage
  offersPage = OffersPage
  downloadPage = DownloadPage;
  tabs = [];


  // tabs = [
  //   { title: "Todo", rootString: "todoPage", icon: "refresh-circle", filter: "None" },
  //   { title: "Audio", rootString: "templatePage", icon: "custom-audio", filter: "AUDIO" },
  //   { title: "Celulares", rootString: "templatePage", icon: "custom-phone", filter: "CELULARES" },
  //   { title: "Electro Hogar", rootString: "templatePage", icon: "custom-electroHogar", filter: "ELECTRO HOGAR" },
  //   { title: "Televisores y Video Juegos", rootString: "templatePage", icon: "custom-tv", filter: "TELEVISORES Y VIDEOJUEGOS" },
  //   { title: "Más", rootString: "templatePage", icon: "custom-boxes", filter: "MAS" },
  //   { title: "Ofertas", rootString: "templatePage", icon: "custom-pricetags", filter: "None" },
  //   { title: "Actualizar", rootString: "downloadPage", icon: "download" },
  // ];



  constructor(
    public plt: Platform,
    private app: AppVersion,
    public navCtrl: NavController,
    private http: HTTP,
    private appData: DataProvider,
  ) {
    this.tabs = this.appData.tabInfo
    this.checkVersion()
    this.modifyTabs()
  }

  ionViewWillEnter() {
    this.checkVersion()
  }
  ionSelected() {
    this.clickCount++
    if (this.clickCount > 5) {
      this.showBug = true
    }

  }

  ionViewDidEnter() {
    if (this.dynamicTabs) {
      this.tabRef.select(this.tabs.length - 1);
    }
  }

  modifyTabs() {
    if (this.firstTime) {
      this.tabs = this.appData.tabInfo
      console.log("entered modifyTabs")
      for (var i = 0; i < this.tabs.length; i++) {
        var name = this.tabs[i].rootString
        if (name == "templatePage") {
          this.tabs[i]["root"] = this.templatePage
        }
        if (name == "downloadPage") {
          this.tabs[i]["root"] = this.downloadPage
        }
        if (name == "todoPage") {
          this.tabs[i]["root"] = this.todoPage
        }


        //////////

        if (name == "audioPage") {
          this.tabs[i]["root"] = this.audioPage
        }
        if (name == "celularesPage") {
          this.tabs[i]["root"] = this.celularesPage
        }
        if (name == "electroHogarPage") {
          this.tabs[i]["root"] = this.electroHogarPage
        }
        if (name == "tvvjPage") {
          this.tabs[i]["root"] = this.tvvjPage
        }
        if (name == "masPage") {
          this.tabs[i]["root"] = this.masPage
        }
        if (name == "offersPage") {
          this.tabs[i]["root"] = this.offersPage
        }
      }
      if (this.dynamicTabs) {
        this.showTabs = true;

      }
      this.firstTime = false
    }
  }

  pagePrep(enteredPage, tabData) {
    console.log("&&&&&&ˆˆˆˆˆˆ&&&&&&&ˆˆˆˆˆˆˆˆ&&&&&&&ˆˆˆˆˆˆ")
    console.log(enteredPage)
    this.modifyTabs()
    this.appData.tabData = tabData
  }

  async endpoint() {
    let url = "https://us-central1-commercetoolstottus.cloudfunctions.net/app/healthTabletApp"
    let returnValue = null;
    await this.http.get(url, {}, {})
      .then(async (data) => {
        console.log("results from endpoint")
        returnValue = JSON.parse(data.data)
      })
    return returnValue.results
  }

  async checkVersion() {

    //get ultimate/minimum
    // console.log(this.plt.versions());
    // alert(this.plt.versions().toString())
    // this.app.getAppName().then(x=>{
    //   console.log("AppName = ",x)
    // });
    // this.app.getPackageName().then(x=>{
    //   console.log("PackageName = ",x)
    // });;
    // this.app.getVersionCode().then(x=>{
    //   console.log("VersionCode = ",x)
    // });;
    let appVersion = null;
    await this.app.getVersionNumber().then(async (x) => {
      console.log("VersionNumber = ", x)
      appVersion = x
    });;
    let endpointResult = null
    endpointResult = await this.endpoint();
    let minVersion = endpointResult["minimalVersion"]
    let ultVersion = endpointResult["ultimateVersion"]

    if (appVersion < minVersion) {
      console.log("THE APP IS TOO OLD")
      this.navCtrl.setRoot(BlockerPage, {
        minVersion: minVersion,
        ultVersion: ultVersion,
        appVersion: appVersion
      })

    } else {
      console.log("THE APP IS JUST RIGHT")
    }
  }
}
